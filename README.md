Chantry redesign
================

Rewrite clojure/thm (server&client for playing 13 Days) in modern Javascript,
modernizing the UI in process.  Requires thm to be running for the game server;
server here talks to thm via redis.


Back-end: express, socket.io.  Front-end: redux-toolkit, bespoke SCSS.


## Setting up

First, clone and set up https://bitbucket.org/arry/thm/src/master/


In `server`:

`npm i`

Copy `server/src/config.js.example` to `server/src/config.js` and modify
according to your Mailgun creds and local dev preferences.


In `client`:

`npm i`

### test users

If you don't want the hassle with mailgun account, you can create a couple of
test users.


In `server`, invoke `node ./script/gen-password.js chga10`.

In another shell, invoke `psql -d thm`[1] and paste the `insert` expression there.

([1] Though I don't remember the precise syntax of invoking `psql` with the user `thm`.)

Now you can log in as `chga10` (password is the same).

### ignore bulk changes in blame

```
git config --global blame.ignoreRevsFile .git-blame-ignore-revs
```


## Starting

1. Make sure that `thm` is running (and `(go)` is invoked in
the Repl)

2. Shell in `server` with `npm start`

3. Shell in `client` with `npm start`

http://localhost:3000 should open automatically with the site.
