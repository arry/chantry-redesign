import { useState, useCallback } from 'react';

export const usernames = [
  'direwoolf0',
  'DivorcedDad0',
  'd3m0nz1',
  'damaxxx2',
  'DeathMuffins3',
  'DerSebomat794',
  'Devilcave5',
  'Dilly736',
  'direwoolf7',
  'DivorcedDad8',
  'd3m0nz9',
  'damaxxx10',
  'DeathMuffins11',
  'DerSebomat7912',
  'Devilcave13',
  'Dilly7314',
  'direwoolf15',
  'DivorcedDad16',
  'dom23ngo17',
  'DomiSmasher18',
  'DrakeSpirit19',
];

export const initialMessages = [
  {
    messageCode: 'chat',
    createdAt: '21:23',
    args: {
      author: 'Alvad20',
      text: 'sorry to my opponent, i have blocked in the manual mode',
    },
  },
  {
    messageCode: 'chat',
    createdAt: '21:33',
    args: {
      author: 'antoniotagliare',
      text: 'sidewinder what are you doing',
    },
  },
  {
    messageCode: 'chat',
    createdAt: '21:23',
    args: {
      author: 'Alvad',
      text: 'sorry to my opponent, i have blocked in the manual mode',
    },
  },
  {
    messageCode: 'chat',
    createdAt: '21:33',
    args: {
      author: 'antoniotagliare',
      text: 'sidewinder what are you doing',
    },
  },
  {
    messageCode: 'chat',
    createdAt: '21:35',
    args: {
      author: 'sidewinder',
      text: "Sorry, I didn't know I was in a game already and was AFK. My bad. sorry to whom ever i was playing i got disconnected - You probably would of won but GG thx",
    },
  },
  {
    messageCode: 'chat',
    createdAt: '21:37',
    args: {
      author: 'Psyclone187',
      text: 'sorry to whom ever i was playing i got disconnected',
    },
  },
  {
    messageCode: 'chat',
    createdAt: '21:37',
    args: {
      author: 'Psyclone187',
      text: 'You probably would of won but GG thx',
    },
  },
];

export const sampleGames = [
  {
    gid: 312,
    code: 'td',
    kind: 'async',
    status: 'running',
    createdAt: new Date(), // string
    // ownerUsername
    comment: 'Anybody welcome',
    // gameSettings -> config
    players: ['arry', 'Taupopscalius'],
    // lastModifiedAt: string
    // runtimeInfo: step, timing (turn, phase, round, active-player), current-players (isYourTurn), player-name->role
    // outcome
    shortName: 'td-312',
  },
  {
    gid: 1602,
    code: 'td',
    kind: 'async',
    status: 'running',
    createdAt: '2020-01-24T15:44:41.945Z',
    ownerUsername: 'Kamquat',
    comment: 'next thowell v. Kamquat game',
    gameSettings: {
      'side-selection': 'random',
    },
    players: ['Kamquat', 'thowell'],
    lastModifiedAt: '2020-02-14T13:14:49.087Z',
    runtimeInfo: {
      'step': 46,
      'timing': {
        'turn': 3,
        'phase': 'strategy',
        'round': 3,
        'active-player': 's',
      },
      'current-players': ['thowell'],
      'player-name->role': {
        Kamquat: 's',
        thowell: 'u',
      },
    },
    outcome: null,
    shortName: 'td-1602',
  },
  {
    gid: 1624,
    code: 'td',
    kind: 'async',
    status: 'running',
    createdAt: '2020-02-07T12:36:19.510Z',
    ownerUsername: 'BertVI',
    comment: 'This game is reserved to be played by Billtje',
    gameSettings: {
      'side-selection': 'random',
    },
    players: ['BertVI', 'Billtje'],
    lastModifiedAt: '2020-02-14T21:19:35.650Z',
    runtimeInfo: {
      'step': 13,
      'timing': {
        'turn': 3,
        'phase': 'strategy',
        'round': 1,
        'active-player': 's',
      },
      'current-players': ['BertVI'],
      'player-name->role': {
        BertVI: 'u',
        Billtje: 's',
      },
    },
    outcome: null,
    shortName: 'td-1624',
  },
  {
    gid: 1631,
    code: 'td',
    kind: 'async',
    status: 'running',
    createdAt: '2020-02-14T08:54:59.146Z',
    ownerUsername: 'BertVI',
    comment: '',
    gameSettings: {
      'side-selection': 'owner-s',
    },
    players: ['BertVI', 'MikeM'],
    lastModifiedAt: '2020-02-14T19:11:18.268Z',
    runtimeInfo: {
      'step': 4,
      'timing': {
        'turn': 1,
        'phase': 'strategy',
        'round': 1,
        'active-player': 'u',
      },
      'current-players': ['BertVI'],
      'player-name->role': {
        MikeM: 'u',
        BertVI: 's',
      },
    },
    outcome: null,
    shortName: 'td-1631',
  },
  {
    gid: 1632,
    code: 'td',
    kind: 'async',
    status: 'open',
    createdAt: '2020-03-03T18:36:57.090Z',
    ownerUsername: 'arry',
    comment: '',
    gameSettings: {
      'side-selection': 'random',
    },
    players: ['arry'],
    lastModifiedAt: '2020-03-03T18:36:57.090Z',
    runtimeInfo: null,
    outcome: null,
    shortName: 'td-1632',
  },
];
