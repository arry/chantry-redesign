const englishTexts = {
  serverError:
    'There was an error connecting to the server. Please try again later.',
};

export function text(code) {
  return englishTexts[code] || code;
}
