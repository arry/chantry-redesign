import React from 'react';

export function GameMenuChoice(props) {
  const { choice } = props;
  return <>{choice.label}</>;
}
