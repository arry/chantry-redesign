import React from 'react';

export const PopupMenuContext = React.createContext({
  isOpen: false,
  target: null,
  open: () => {},
  close: () => {},
});
PopupMenuContext.displayName = 'PopupMenuContext';
