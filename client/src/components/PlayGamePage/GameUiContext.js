import React from 'react';

export const GameUiContext = React.createContext({
  shortName: null,
  cs: null,
});
GameUiContext.displayName = 'GameUiContext';
