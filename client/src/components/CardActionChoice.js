import React from 'react';

export function CardActionChoice(props) {
  const { choice } = props;
  return <>{choice.label}</>;
}
