import { useState, useCallback } from 'react';

export function useTap(onClick) {
  return {
    onClick,
  };
}
