import { createBrowserHistory } from 'history';
import { onLocationChanged } from 'connector';
import { store } from 'store';

export const history = createBrowserHistory();

history.listen((location, action) => {
  onLocationChanged(location, store.dispatch);
});
