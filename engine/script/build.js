const esbuild = require('esbuild');

const watch = process.argv[2] === '--watch';

esbuild
  .build({
    entryPoints: ['src/main.ts'],
    bundle: true,
    outfile: 'dist/main.js',
    platform: 'node',
    watch,
    logLevel: 'info',
    plugins: [],
    sourcemap: true,
  })
  .catch(() => process.exit(1));
