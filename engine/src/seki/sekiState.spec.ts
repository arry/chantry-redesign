import { createSekiState } from './sekiState';
import { determineMovementTargets } from './sekiUtils';
import { TestEngine } from '../test/TestEngine';
import { fa } from '../foundationActions';
import { sekia } from './sekiActions';
import { qualifiedName } from '../utils';

describe('createSekiState', () => {
  test('create with owner-i', () => {
    const descriptor = {
      players: ['John', 'Bill'],
      gameSettings: {
        ['side-selection']: 'owner-i' as 'owner-i',
      },
    };
    const engine = new TestEngine(createSekiState(descriptor));
    const rep = engine.getStateRep('spec', { section: 'players' });
    expect(rep).toEqual(`Players (2)
- i John
- t Bill
`);
  });

  test('create with owner-t', () => {
    const descriptor = {
      players: ['John', 'Bill'],
      gameSettings: {
        ['side-selection']: 'owner-t' as 'owner-t',
      },
    };
    const engine = new TestEngine(createSekiState(descriptor));
    const rep = engine.getStateRep('spec', { section: 'players' });
    expect(rep).toEqual(`Players (2)
- t John
- i Bill
`);
  });
});

const descriptor = {
  players: ['John', 'Bill'],
  gameSettings: {
    ['side-selection']: 'owner-t' as 'owner-t',
  },
};

describe('determineMovementTargets', () => {
  let engine;
  const hugeStackSids = [
    // small
    'toarno:1',
    'tocano:1',
    'tocano:2',
    'toelno:1',
    // medium -1
    'toelno:2',
    'toelno:3',
    'toelno:4',
    'toreno:1',
    // large -2
    'toreno:2',
    'toreno:3',
    'toreno:4',
    'maarno:1',
    // huge -3
    'macano:1',
  ];
  const role = 't';

  function bringStack(sids, locationId) {
    return sekia.moveUnitsBySids(sids, qualifiedName(role, locationId));
  }

  function bringLeader(locationId) {
    return sekia.moveUnitsBySids(['fulest:1'], qualifiedName(role, locationId));
  }

  function prepareForceMarch() {
    return sekia.drawCardsLowLevel(role, 1);
  }

  beforeEach(() => {
    engine = new TestEngine(createSekiState(descriptor));
    engine.act(
      sekia.moveAllCards('t:hand', 't:discard'),
      sekia.moveAllCards('i:kyoto', 'i:dead'),
      sekia.moveAllCards('i:gifu', 'i:dead'),
      sekia.moveAllCards('i:ueda', 'i:dead'),
      sekia.moveAllCards('t:anotsu', 't:dead')
    );
  });

  describe('huge stack: 13 units, -3 penalty', () => {
    const sids = hugeStackSids;

    test('with no bonuses', () => {
      const locationId = 'matsum';

      engine.act(bringStack(sids, locationId));
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([]);
    });

    test('with just highway', () => {
      const locationId = 'osaka';
      engine.act(bringStack(sids, locationId));

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );

      expect(targets).toStrictEqual([]);
    });

    test('with highway and forced march', () => {
      const locationId = 'osaka';
      engine.act(bringStack(sids, locationId), prepareForceMarch());

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([]);
    });

    test('with highway, leadership (auto) and forced march', () => {
      const locationId = 'okazak';
      engine.act(bringStack(sids, locationId), prepareForceMarch());

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'hamama',
          passingLocations: [],
          h: true,
          al: true,
          dl: false,
          dlfmChoice: false,
          fm: true,
          hApplicable: true,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
        {
          sourceLocationId: locationId,
          targetLocationId: 'kiyosu',
          passingLocations: [],
          h: true,
          al: true,
          dl: false,
          dlfmChoice: false,
          fm: true,
          hApplicable: true,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
      ]);
    });

    test('with highway, leadership (declared) and forced march', () => {
      const locationId = 'osaka';
      engine.act(
        bringStack(sids, locationId),
        bringLeader(locationId),
        prepareForceMarch()
      );
      const targets = determineMovementTargets(
        engine.state,
        role,
        'osaka',
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'kyoto',
          passingLocations: [],
          h: true,
          al: false,
          dl: true,
          dlfmChoice: false,
          fm: true,
          hApplicable: true,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
      ]);
    });

    test('with highway, leadership (declared) and forced march; overrun', () => {
      const locationId = 'osaka';
      engine.act(
        bringStack(sids, locationId),
        bringLeader(locationId),
        prepareForceMarch(),
        sekia.moveUnitsBySids(['koarno:1', 'koarno:2'], 'i:kyoto')
      );
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'kyoto',
          hasOverrun: true,
        }),
      ]);
    });

    test('with highway, leadership (declared) and forced march; no overrun', () => {
      const locationId = 'osaka';
      engine.act(
        bringStack(sids, locationId),
        bringLeader(locationId),
        prepareForceMarch(),
        sekia.moveUnitsBySids(
          ['koarno:1', 'koarno:2', 'koelno:1', 'koelno:2'],
          'i:kyoto'
        )
      );
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'kyoto',
          hasOverrun: false,
        }),
      ]);
    });
  });

  describe('large stack: 9 units, -2 penalty', () => {
    const sids = hugeStackSids.slice(0, 9);

    test('with no bonuses', () => {
      const locationId = 'matsum';
      engine.act(bringStack(sids, locationId));

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([]);
    });

    test('with just highway', () => {
      const locationId = 'osaka';
      engine.act(bringStack(sids, locationId));

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([]);
    });

    test('with just leadership', () => {
      const locationId = 'matsum';

      engine.act(bringStack(sids, locationId), bringLeader(locationId));
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([]);
    });

    test('with just forced march', () => {
      const locationId = 'matsum';
      engine.act(bringStack(sids, locationId), prepareForceMarch());
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([]);
    });

    test('with highway and leadership (auto)', () => {
      const locationId = 'okazak';

      engine.act(bringStack(sids, locationId));
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'hamama',
          passingLocations: [],
          h: true,
          al: true,
          dl: false,
          dlfmChoice: false,
          fm: false,
          hApplicable: true,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'kiyosu',
        }),
      ]);
    });

    test('with highway and leadership (declared)', () => {
      const locationId = 'osaka';

      engine.act(bringStack(sids, locationId), bringLeader(locationId));
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'kyoto',
          passingLocations: [],
          h: true,
          al: false,
          dl: true,
          fm: false,
          dlfmChoice: false,
          hApplicable: true,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
      ]);
    });

    test('with highway and forced march', () => {
      const locationId = 'osaka';
      engine.act(bringStack(sids, locationId), prepareForceMarch());

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'kyoto',
          passingLocations: [],
          h: true,
          hApplicable: true,
          al: false,
          dl: false,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
        }),
      ]);
    });

    test('with leadership (auto) and forced march', () => {
      const locationId = 'anotsu';

      engine.act(bringStack(sids, locationId), prepareForceMarch());

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'kumano',
          passingLocations: [],
          h: false,
          hApplicable: false,
          al: true,
          dl: false,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'kuwana',
        }),
      ]);
    });

    test('with leadership (declared) and forced march', () => {
      const locationId = 'matsum';

      engine.act(
        bringStack(sids, locationId),
        bringLeader(locationId),
        prepareForceMarch()
      );

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'arato',
          passingLocations: [],
          h: false,
          hApplicable: false,
          al: false,
          dl: true,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'kiso',
        }),
      ]);
    });

    test('with highway, leadership (auto) and forced march', () => {
      const locationId = 'okazak';

      engine.act(bringStack(sids, locationId), prepareForceMarch());

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );

      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'hamama',
          passingLocations: [],
          h: true,
          hApplicable: true,
          al: true,
          dl: false,
          fm: false,
          dlfmChoice: false,
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'kiyosu',
        }),
        expect.objectContaining({
          targetLocationId: 'sunpu',
          passingLocations: [{ locationId: 'hamama', hasOverrun: false }],
          h: true,
          hApplicable: true,
          al: true,
          dl: false,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
        }),
        // no Gifu because Kyosu-Gifu is not a highway
        expect.objectContaining({
          targetLocationId: 'kuwana',
        }),
      ]);
    });

    test('with highway, leadership (declared), and forced march', () => {
      const locationId = 'hamama';

      engine.act(
        bringStack(sids, locationId),
        bringLeader(locationId),
        prepareForceMarch()
      );

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );

      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'okazak',
          passingLocations: [],
          h: true,
          hApplicable: true,
          al: false,
          dl: false,
          fm: false,
          dlfmChoice: true,
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'sunpu',
        }),
        expect.objectContaining({
          targetLocationId: 'kiyosu',
          passingLocations: [{ locationId: 'okazak', hasOverrun: false }],
          h: true,
          hApplicable: true,
          al: false,
          dl: true,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'numazu',
          passingLocations: [expect.objectContaining({ locationId: 'sunpu' })],
        }),
      ]);
    });
  });

  describe('medium stack: 5 units, -1 penalty', () => {
    const sids = hugeStackSids.slice(0, 5);

    test('no bonuses', () => {
      const locationId = 'matsum';
      engine.act(bringStack(sids, locationId));

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([]);
    });

    test('with just highway', () => {
      const locationId = 'osaka';
      engine.act(bringStack(sids, locationId));

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'kyoto',
          passingLocations: [],
          h: true,
          al: false,
          dl: false,
          dlfmChoice: false,
          fm: false,
          hApplicable: true,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
      ]);
    });

    test('with just leadership (auto)', () => {
      const locationId = 'anotsu';

      engine.act(bringStack(sids, locationId));
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'kumano',
          passingLocations: [],
          h: false,
          hApplicable: false,
          al: true,
          dl: false,
          fm: false,
          dlfmChoice: false,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'kuwana',
        }),
      ]);
    });

    test('with just leadership (declared)', () => {
      const locationId = 'matsum';

      engine.act(bringStack(sids, locationId), bringLeader(locationId));

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'arato',
          passingLocations: [],
          h: false,
          hApplicable: false,
          al: false,
          dl: true,
          fm: false,
          dlfmChoice: false,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'kiso',
        }),
      ]);
    });

    test('with just forced march', () => {
      const locationId = 'matsum';

      engine.act(bringStack(sids, locationId), prepareForceMarch());

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'arato',
          passingLocations: [],
          h: false,
          hApplicable: false,
          al: false,
          dl: false,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'kiso',
        }),
      ]);
    });

    test('with highway and leadership (auto)', () => {
      const locationId = 'okazak';

      engine.act(bringStack(sids, locationId));
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'hamama',
          passingLocations: [],
          h: true,
          hApplicable: true,
          al: false,
          dl: false,
          dlfmChoice: false,
          fm: false,
          isFinal: false,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'kiyosu',
        }),
        expect.objectContaining({
          targetLocationId: 'sunpu',
          passingLocations: [{ locationId: 'hamama', hasOverrun: false }],
          h: true,
          hApplicable: true,
          al: true,
          dl: false,
          fm: false,
          dlfmChoice: false,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'kuwana',
          passingLocations: [{ locationId: 'kiyosu', hasOverrun: false }],
        }),
      ]);
    });

    test('with highway and leadership (declared)', () => {
      const locationId = 'hamama';

      engine.act(bringStack(sids, locationId), bringLeader(locationId));
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'okazak',
          passingLocations: [],
          h: true,
          hApplicable: true,
          al: false,
          dl: false,
          dlfmChoice: false,
          fm: false,
          isFinal: false,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'sunpu',
        }),
        expect.objectContaining({
          targetLocationId: 'kiyosu',
          passingLocations: [{ locationId: 'okazak', hasOverrun: false }],
          h: true,
          hApplicable: true,
          al: false,
          dl: true,
          fm: false,
          dlfmChoice: false,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'numazu',
          passingLocations: [{ locationId: 'sunpu', hasOverrun: false }],
        }),
      ]);
    });

    test('with highway and forced march', () => {
      const locationId = 'hamama';

      engine.act(bringStack(sids, locationId), prepareForceMarch());
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'okazak',
          passingLocations: [],
          h: true,
          hApplicable: true,
          al: false,
          dl: false,
          dlfmChoice: false,
          fm: false,
          isFinal: false,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'sunpu',
        }),
        expect.objectContaining({
          targetLocationId: 'kiyosu',
          passingLocations: [{ locationId: 'okazak', hasOverrun: false }],
          h: true,
          hApplicable: true,
          al: false,
          dl: false,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'numazu',
          passingLocations: [{ locationId: 'sunpu', hasOverrun: false }],
        }),
      ]);
    });

    test('with leadership (auto) and forced march', () => {
      const locationId = 'anotsu';

      engine.act(bringStack(sids, locationId), prepareForceMarch());
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'kumano',
          passingLocations: [],
          h: false,
          hApplicable: false,
          al: true,
          dl: false,
          dlfmChoice: false,
          fm: false,
          isFinal: false,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'kuwana',
        }),
        expect.objectContaining({
          sourceLocationId: locationId,
          targetLocationId: 'tanabe',
          passingLocations: [{ locationId: 'kumano', hasOverrun: false }],
          h: false,
          hApplicable: false,
          al: true,
          dl: false,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'kiyosu',
        }),
        expect.objectContaining({
          targetLocationId: 'minaku',
        }),
      ]);
    });

    test('with leadership (declared) and forced march', () => {
      const locationId = 'matsum';

      engine.act(
        bringStack(sids, locationId),
        bringLeader(locationId),
        prepareForceMarch()
      );
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'arato',
          passingLocations: [],
          h: false,
          hApplicable: false,
          al: false,
          dl: false,
          dlfmChoice: true,
          fm: false,
          isFinal: false,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'kiso',
        }),
        expect.objectContaining({
          targetLocationId: 'takeda',
          passingLocations: [{ locationId: 'arato', hasOverrun: false }],
          h: false,
          hApplicable: false,
          al: false,
          dl: true,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'ueda',
        }),
        expect.objectContaining({
          targetLocationId: 'iwamur',
        }),
        expect.objectContaining({
          targetLocationId: 'suwa',
        }),
      ]);
    });

    test('with highway, leadership (auto), and forced march', () => {
      const locationId = 'okazak';

      engine.act(bringStack(sids, locationId), prepareForceMarch());
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'hamama',
          passingLocations: [],
          h: true,
          hApplicable: true,
          al: false,
          dl: false,
          dlfmChoice: false,
          fm: false,
          isFinal: false,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'kiyosu',
        }),
        expect.objectContaining({
          targetLocationId: 'sunpu',
          passingLocations: [{ locationId: 'hamama', hasOverrun: false }],
          h: true,
          hApplicable: true,
          al: true,
          dl: false,
          fm: false,
          dlfmChoice: false,
          isFinal: false,
          hasOverrun: false,
        }),
        {
          sourceLocationId: locationId,
          targetLocationId: 'gifu',
          passingLocations: [{ locationId: 'kiyosu', hasOverrun: false }],
          h: false,
          hApplicable: false,
          al: true,
          dl: false,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'kuwana',
          passingLocations: [{ locationId: 'kiyosu', hasOverrun: false }],
        }),
        {
          sourceLocationId: locationId,
          targetLocationId: 'numazu',
          passingLocations: [
            { locationId: 'hamama', hasOverrun: false },
            { locationId: 'sunpu', hasOverrun: false },
          ],
          h: true,
          hApplicable: true,
          al: true,
          dl: false,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'minaku',
          passingLocations: [
            { locationId: 'kiyosu', hasOverrun: false },
            { locationId: 'kuwana', hasOverrun: false },
          ],
        }),
      ]);
    });

    test('with highway, leadership (declared), and forced march', () => {
      const locationId = 'hamama';

      engine.act(
        bringStack(sids, locationId),
        bringLeader(locationId),
        prepareForceMarch()
      );
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'okazak',
          passingLocations: [],
          h: true,
          hApplicable: true,
          al: false,
          dl: false,
          dlfmChoice: false,
          fm: false,
          isFinal: false,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'sunpu',
        }),
        expect.objectContaining({
          targetLocationId: 'kiyosu',
          passingLocations: [{ locationId: 'okazak', hasOverrun: false }],
          h: true,
          hApplicable: true,
          al: false,
          dl: false,
          fm: false,
          dlfmChoice: true,
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'numazu',
          passingLocations: [{ locationId: 'sunpu', hasOverrun: false }],
        }),
        {
          sourceLocationId: locationId,
          targetLocationId: 'kuwana',
          passingLocations: [
            { locationId: 'okazak', hasOverrun: false },
            { locationId: 'kiyosu', hasOverrun: false },
          ],
          h: true,
          hApplicable: true,
          al: false,
          dl: true,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({
          targetLocationId: 'hakone',
          passingLocations: [
            { locationId: 'sunpu', hasOverrun: false },
            { locationId: 'numazu', hasOverrun: false },
          ],
        }),
      ]);
    });
  });

  describe('small stack: 1-4 units, 0 penalty', () => {
    const sids = hugeStackSids.slice(0, 1);

    test('no bonuses', () => {
      const locationId = 'matsum';
      engine.act(bringStack(sids, locationId));

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        {
          sourceLocationId: locationId,
          targetLocationId: 'arato',
          passingLocations: [],
          h: false,
          hApplicable: false,
          al: false,
          dl: false,
          dlfmChoice: false,
          fm: false,
          isFinal: true,
          hasOverrun: false,
          ms: false,
        },
        expect.objectContaining({ targetLocationId: 'kiso' }),
      ]);
    });

    test('with just highway', () => {
      const locationId = 'osaka';
      engine.act(bringStack(sids, locationId));
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'himeji',
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'wakaya',
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'kyoto',
          hApplicable: true,
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'minaku',
          passingLocations: [{ locationId: 'kyoto', hasOverrun: false }],
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'sawaya',
          passingLocations: [{ locationId: 'kyoto', hasOverrun: false }],
          isFinal: true,
        }),
      ]);
    });

    test('with just leadership (auto)', () => {
      const locationId = 'anotsu';

      engine.act(bringStack(sids, locationId));
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'kumano',
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'kuwana',
        }),
        expect.objectContaining({
          targetLocationId: 'tanabe',
          passingLocations: [{ locationId: 'kumano', hasOverrun: false }],
          al: true,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'kiyosu',
          passingLocations: [{ locationId: 'kuwana', hasOverrun: false }],
          al: true,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'minaku',
          passingLocations: [{ locationId: 'kuwana', hasOverrun: false }],
          al: true,
          isFinal: true,
        }),
      ]);
    });

    test('with just leadership (declared)', () => {
      const locationId = 'matsum';

      engine.act(bringStack(sids, locationId), bringLeader(locationId));
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'arato',
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'kiso',
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'takeda',
          passingLocations: [{ locationId: 'arato', hasOverrun: false }],
          h: false,
          hApplicable: false,
          al: false,
          dl: true,
          fm: false,
          dlfmChoice: false,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'ueda',
        }),
        expect.objectContaining({
          targetLocationId: 'iwamur',
        }),
        expect.objectContaining({
          targetLocationId: 'suwa',
        }),
      ]);
    });

    test('with just forced march', () => {
      const locationId = 'matsum';

      engine.act(bringStack(sids, locationId), prepareForceMarch());
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'arato',
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'kiso',
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'takeda',
          passingLocations: [{ locationId: 'arato', hasOverrun: false }],
          h: false,
          hApplicable: false,
          al: false,
          dl: false,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'ueda',
        }),
        expect.objectContaining({
          targetLocationId: 'iwamur',
        }),
        expect.objectContaining({
          targetLocationId: 'suwa',
        }),
      ]);
    });

    test('with highway and leadership (auto)', () => {
      const locationId = 'okazak';

      engine.act(bringStack(sids, locationId));
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'hamama',
        }),
        expect.objectContaining({
          targetLocationId: 'kiyosu',
        }),
        expect.objectContaining({
          targetLocationId: 'sunpu',
          passingLocations: [{ locationId: 'hamama', hasOverrun: false }],
          h: true,
          hApplicable: true,
          al: false,
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'gifu',
          h: false,
          al: true,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'kuwana',
        }),
        expect.objectContaining({
          targetLocationId: 'numazu',
          passingLocations: [
            { locationId: 'hamama', hasOverrun: false },
            { locationId: 'sunpu', hasOverrun: false },
          ],
          h: true,
          al: true,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'minaku',
        }),
      ]);
    });

    test('with highway and leadership (declared)', () => {
      const locationId = 'hamama';

      engine.act(bringStack(sids, locationId), bringLeader(locationId));

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'okazak',
        }),
        expect.objectContaining({
          targetLocationId: 'sunpu',
        }),
        expect.objectContaining({
          targetLocationId: 'kiyosu',
          passingLocations: [{ locationId: 'okazak', hasOverrun: false }],
          h: true,
          dl: false,
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'numazu',
          passingLocations: [{ locationId: 'sunpu', hasOverrun: false }],
        }),
        expect.objectContaining({
          targetLocationId: 'kuwana',
          passingLocations: [
            { locationId: 'okazak', hasOverrun: false },
            { locationId: 'kiyosu', hasOverrun: false },
          ],
          h: true,
          dl: true,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'hakone',
          passingLocations: [
            { locationId: 'sunpu', hasOverrun: false },
            { locationId: 'numazu', hasOverrun: false },
          ],
        }),
      ]);
    });

    test('with highway and forced march', () => {
      const locationId = 'hamama';

      engine.act(bringStack(sids, locationId), prepareForceMarch());
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'okazak',
        }),
        expect.objectContaining({
          targetLocationId: 'sunpu',
        }),
        expect.objectContaining({
          targetLocationId: 'kiyosu',
          passingLocations: [{ locationId: 'okazak', hasOverrun: false }],
          h: true,
          dl: false,
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'numazu',
          passingLocations: [{ locationId: 'sunpu', hasOverrun: false }],
        }),
        expect.objectContaining({
          targetLocationId: 'kuwana',
          passingLocations: [
            { locationId: 'okazak', hasOverrun: false },
            { locationId: 'kiyosu', hasOverrun: false },
          ],
          h: true,
          dl: false,
          fm: true,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'hakone',
          passingLocations: [
            { locationId: 'sunpu', hasOverrun: false },
            { locationId: 'numazu', hasOverrun: false },
          ],
        }),
      ]);
    });

    test('with leadership (auto) and forced march', () => {
      const locationId = 'anotsu';

      engine.act(bringStack(sids, locationId), prepareForceMarch());
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        expect.objectContaining({
          targetLocationId: 'kumano',
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'kuwana',
        }),
        expect.objectContaining({
          targetLocationId: 'tanabe',
          passingLocations: [{ locationId: 'kumano', hasOverrun: false }],
          al: true,
          isFinal: false,
        }),
        expect.objectContaining({
          targetLocationId: 'kiyosu',
          passingLocations: [{ locationId: 'kuwana', hasOverrun: false }],
          al: true,
        }),
        expect.objectContaining({
          targetLocationId: 'minaku',
          passingLocations: [{ locationId: 'kuwana', hasOverrun: false }],
          al: true,
        }),
        expect.objectContaining({
          targetLocationId: 'wakaya',
          passingLocations: [
            { locationId: 'kumano', hasOverrun: false },
            { locationId: 'tanabe', hasOverrun: false },
          ],
          al: true,
          fm: true,
          isFinal: true,
        }),
        expect.objectContaining({
          targetLocationId: 'gifu',
        }),
        expect.objectContaining({
          targetLocationId: 'okazak',
        }),
        expect.objectContaining({
          targetLocationId: 'kyoto',
        }),
      ]);
    });

    test('with leadership (declared) and forced march', () => {
      const locationId = 'matsum';

      engine.act(
        bringStack(sids, locationId),
        bringLeader(locationId),
        prepareForceMarch()
      );
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        // 1a
        expect.objectContaining({
          targetLocationId: 'arato',
          isFinal: false,
        }),
        // 1b
        expect.objectContaining({
          targetLocationId: 'kiso',
          isFinal: false,
        }),
        // 1a.1
        expect.objectContaining({
          targetLocationId: 'takeda',
          passingLocations: [{ locationId: 'arato', hasOverrun: false }],
          h: false,
          hApplicable: false,
          al: false,
          dl: false,
          fm: false,
          dlfmChoice: true,
          isFinal: false,
        }),
        // 1a.2
        expect.objectContaining({
          targetLocationId: 'ueda',
        }),
        // 1b.1
        expect.objectContaining({
          targetLocationId: 'iwamur',
        }),
        // 1b.2
        expect.objectContaining({
          targetLocationId: 'suwa',
        }),
        // 1a.1.1
        expect.objectContaining({
          targetLocationId: 'nagaok',
          passingLocations: [
            { locationId: 'arato', hasOverrun: false },
            { locationId: 'takeda', hasOverrun: false },
          ],
          h: false,
          hApplicable: false,
          al: false,
          dl: true,
          fm: true,
          dlfmChoice: false,
          isFinal: true,
        }),
        // 1a.1.2
        expect.objectContaining({
          targetLocationId: 'toyama',
          passingLocations: [
            { locationId: 'arato', hasOverrun: false },
            { locationId: 'takeda', hasOverrun: false },
          ],
          isFinal: true,
        }),
        // 1a.2.1
        expect.objectContaining({
          targetLocationId: 'saku',
          passingLocations: [
            { locationId: 'arato', hasOverrun: false },
            { locationId: 'ueda', hasOverrun: false },
          ],
        }),
        // 1b.1.1
        expect.objectContaining({
          targetLocationId: 'gifu',
        }),
        // 1b.2.1
        expect.objectContaining({
          targetLocationId: 'kofu',
        }),
        // 1b.2.2
        expect.objectContaining({
          targetLocationId: 'saku',
          passingLocations: [
            { locationId: 'kiso', hasOverrun: false },
            { locationId: 'suwa', hasOverrun: false },
          ],
        }),
      ]);
    });

    test('with highway, leadership (auto), and forced march', () => {
      const locationId = 'okazak';

      engine.act(bringStack(sids, locationId), prepareForceMarch());

      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );

      expect(targets).toStrictEqual([
        // 1a
        expect.objectContaining({
          targetLocationId: 'hamama',
        }),
        // 1b
        expect.objectContaining({
          targetLocationId: 'kiyosu',
        }),
        // 1a.1
        expect.objectContaining({
          targetLocationId: 'sunpu',
          passingLocations: [{ locationId: 'hamama', hasOverrun: false }],
        }),
        // 1b.1
        expect.objectContaining({
          targetLocationId: 'gifu',
          passingLocations: [{ locationId: 'kiyosu', hasOverrun: false }],
          h: false,
          hApplicable: false,
          al: true,
          isFinal: false,
        }),
        // 1b.2
        expect.objectContaining({
          targetLocationId: 'kuwana',
        }),
        // 1a.1.1
        expect.objectContaining({
          targetLocationId: 'numazu',
          passingLocations: [
            { locationId: 'hamama', hasOverrun: false },
            { locationId: 'sunpu', hasOverrun: false },
          ],
        }),
        // 1b.1.1
        expect.objectContaining({
          targetLocationId: 'iwamur',
          passingLocations: [
            { locationId: 'kiyosu', hasOverrun: false },
            { locationId: 'gifu', hasOverrun: false },
          ],
          al: true,
          fm: true,
          isFinal: true,
        }),
        // 1b.1.2
        expect.objectContaining({
          targetLocationId: 'sawaya',
        }),
        // 1b.2.1
        expect.objectContaining({
          targetLocationId: 'anotsu',
          isFinal: true,
        }),
        // 1b.2.2
        expect.objectContaining({
          targetLocationId: 'minaku',
          isFinal: false,
        }),
        // 1a.1.1.1
        expect.objectContaining({
          targetLocationId: 'hakone',
          passingLocations: [
            { locationId: 'hamama', hasOverrun: false },
            { locationId: 'sunpu', hasOverrun: false },
            { locationId: 'numazu', hasOverrun: false },
          ],
          h: true,
          al: true,
          fm: true,
          isFinal: true,
        }),
        // 1b.2.1.1
        expect.objectContaining({
          targetLocationId: 'kyoto',
          passingLocations: [
            { locationId: 'kiyosu', hasOverrun: false },
            { locationId: 'kuwana', hasOverrun: false },
            { locationId: 'minaku', hasOverrun: false },
          ],
          h: true,
          al: true,
          fm: true,
          isFinal: true,
        }),
      ]);
    });

    test('with highway, leadership (declared), and forced march', () => {
      const locationId = 'hamama';

      engine.act(
        bringStack(sids, locationId),
        bringLeader(locationId),
        prepareForceMarch()
      );
      const targets = determineMovementTargets(
        engine.state,
        role,
        locationId,
        sids
      );
      expect(targets).toStrictEqual([
        // 1a
        expect.objectContaining({
          targetLocationId: 'okazak',
        }),
        // 1b
        expect.objectContaining({
          targetLocationId: 'sunpu',
        }),
        // 1a.1
        expect.objectContaining({
          targetLocationId: 'kiyosu',
          h: true,
        }),
        // 1b.1
        expect.objectContaining({
          targetLocationId: 'numazu',
        }),
        // 1a.1.1
        expect.objectContaining({
          targetLocationId: 'gifu',
          isFinal: true,
          dl: true,
          fm: true,
        }),
        // 1a.1.2
        expect.objectContaining({
          targetLocationId: 'kuwana',
          isFinal: false,
          h: true,
          dlfmChoice: true,
        }),
        // 1b.1.1
        expect.objectContaining({
          targetLocationId: 'kofu',
          isFinal: true,
          h: false,
          dl: true,
          fm: true,
        }),
        // 1b.1.2
        expect.objectContaining({
          targetLocationId: 'hakone',
          isFinal: false,
          h: true,
          dlfmChoice: true,
        }),
        // 1a.1.2.1
        expect.objectContaining({
          targetLocationId: 'minaku',
          isFinal: true,
        }),
        // 1b.1.2.1
        expect.objectContaining({
          targetLocationId: 'edo',
          isFinal: true,
        }),
      ]);
    });
  });

  test('continuing', () => {});
});
