import { CardInRegion, Role } from '../types';

export type PassingLocation = {
  locationId: string;
  hasOverrun?: boolean;
  counted?: boolean;
};

export type MovementTargetInBuilding = {
  targetLocationId: string;
  h: boolean; // highway bonus will be used
  al: boolean; // automatic leadership will be used (can be fancy and show
  // either capital or castle icon, so it won't be a boolean)
  dl: boolean; // declared leadership will be used - exclusive with al.
  fm: boolean; // forced march will be used; hence should reply with the payment cid
  dlfmChoice: boolean; // must use either dl or fm; when this is true both dl
  // and fm will be false.

  hApplicable: boolean; // for continuing
};

export type MovementTarget = MovementTargetInBuilding & {
  sourceLocationId: string;
  passingLocations: PassingLocation[];
  ms: boolean; // if true, then must stop for meeting enemy (also sets isFinal)
  hasOverrun: boolean;
  isFinal: boolean; // if true, then no more movement is possible - so must
  // "drop off" everybody
};

export type CombatOutcome = {
  tImpact: number;
  iImpact: number;
  whoIsWinning: string;
  whoIsLosing: string;
  iLosses: number;
  tLosses: number;
};

export type RCO = {
  role: string;
  impact: number;
  actualNLosses: number;
  nReplenishedCards: number;
};

export type RetreatConfiguration = {
  unitCards: CardInRegion[];
  locationIds: string[];
  isCastleRetreatPossible: boolean;
};

export type UnpredictedCombatConfiguration = {
  locationId: string;
  role: Role;
};
