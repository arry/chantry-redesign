import {
  players,
  smallZoneDescriptors,
  createWithPlayers,
} from './test/testData';
import { TestEngine } from './test/TestEngine';
import { createEmptyState } from './creation';
import { createMvState } from './mv/mvState';
import { fa, getStateAfterActions } from './foundationActions';

describe('stateToRep', () => {
  describe('empty state', () => {
    const engine = new TestEngine(createEmptyState('mv'));

    test('all sections', () => {
      const rep = engine.getStateRep('spec');
      expect(rep).toEqual(`You: spec

Players (0)

Vars

Zones (0)
`);
    });

    test('"you" section', () => {
      const rep = engine.getStateRep('spec', { section: 'you' });
      expect(rep).toEqual(`You: spec\n`);
    });

    test('"vars" section', () => {
      const rep = engine.getStateRep('spec', { section: 'vars' });
      expect(rep).toEqual(`Vars\n`);
    });

    test('players section', () => {
      const rep = engine.getStateRep('spec', { section: 'players' });
      expect(rep).toEqual(`Players (0)\n`);
    });

    test('zones section', () => {
      const rep = engine.getStateRep('spec', { section: 'zones' });
      expect(rep).toEqual(`Zones (0)\n`);
    });
  });

  describe('state with players', () => {
    const engine = new TestEngine(createWithPlayers());

    test('for spec', () => {
      const rep = engine.getStateRep('spec', { section: 'players' });
      expect(rep).toEqual(`Players (2)
- aa John
- bb Bill
`);
    });

    test('for player', () => {
      const rep = engine.getStateRep('aa', { section: 'players' });
      expect(rep).toEqual(`Players (2)
- aa John
- bb Bill
`);
    });
  });

  test('state with players', () => {
    const engine = new TestEngine(
      createMvState({
        players: ['John', 'Bill', 'Kate'].map((playerName) => ({
          playerName,
        })),
        skipInitialVars: true,
      })
    );

    const rep = engine.getStateRep('spec');

    expect(rep).toEqual(`You: spec

Players (3)
- aa John
- bb Bill
- cc Kate

Vars

Zones (0)
`);
  });

  describe('state with empty zones', () => {
    const engine = new TestEngine(
      createMvState({
        players,
        zones: smallZoneDescriptors,
        skipInitialVars: true,
      })
    );

    test('for spec', () => {
      const rep = engine.getStateRep('spec');
      expect(rep).toEqual(`You: spec\n\nPlayers (2)
- aa John
- bb Bill

Vars

Zones (8)
- aa:hand bi aa (0)
- aa:library bi none (0)
- aa:support ra all (0)
- aa:uncontrolled ri aa (0)
- bb:hand bi bb (0)
- bb:library bi none (0)
- bb:support ra all (0)
- bb:uncontrolled ri bb (0)
`);
    });

    test('for player', () => {
      const rep = engine.getStateRep('aa');
      expect(rep).toEqual(`You: aa\n\nPlayers (2)
- aa John
- bb Bill

Vars

Zones (8)
- aa:hand ba aa (0)
- aa:library bi none (0)
- aa:support ra all (0)
- aa:uncontrolled ra aa (0)
- bb:hand bi bb (0)
- bb:library bi none (0)
- bb:support ra all (0)
- bb:uncontrolled ri bb (0)
`);
    });
  });

  describe('state with some cards', () => {
    const engine = new TestEngine(
      getStateAfterActions(
        createWithPlayers(),
        fa.createZone('bunch', 'aa', 'hand', ['aa'], 'library'),
        fa.createZone('region', 'aa', 'uncontrolled', ['aa'], 'crypt'),
        fa.createCard('krc', 'library', 'aa:hand'),
        fa.createCard('krc', 'library', 'aa:hand'),
        fa.createCard('ashley', 'crypt', 'aa:uncontrolled', {
          facing: 'facedown',
        }),
        fa.createCard('ashley', 'crypt', 'aa:uncontrolled', {
          facing: 'faceup',
        })
      )
    );

    test('for spec', () => {
      const rep = engine.getStateRep('spec', { section: 'zones' });
      expect(rep).toEqual(`Zones (2)
- aa:hand bi aa (2)
- aa:uncontrolled ri aa (2)
  - Haa#1 dr
  - ashley#1 ur
`);
    });

    test('for owner', () => {
      const rep = engine.getStateRep('aa', { section: 'zones' });
      expect(rep).toEqual(`Zones (2)
- aa:hand ba aa (2)
  - krc#1
  - krc#2
- aa:uncontrolled ra aa (2)
  - ashley#1 dr
  - ashley#2 ur
`);
    });
  });
});
