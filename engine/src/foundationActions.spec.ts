import 'jest';
import { stateToRep } from './rep';
import { createEmptyState } from './creation';
import { fa, getStateAfterActions } from './foundationActions';
import { applyAction } from './actionsInfrastructure';
import { deltaToRep, deltaToVDelta } from './deltas';
import { createWithPlayers, createWithZones } from './test/testData';
import { Region } from './types';
import { TestEngine } from './test/TestEngine';

describe('foundationActions', () => {
  describe('addPlayer action', () => {
    test('should work', () => {
      const engine = new TestEngine();

      engine.act(fa.addPlayer('John', 'aa'));

      expect(
        engine.getStateRep('spec', {
          section: 'players',
        })
      ).toEqual(`Players (1)\n- aa John\n`);

      ['spec', 'aa'].forEach((pov) => {
        expect(engine.getLastDeltaRep(pov)).toEqual(`:playerAdded John aa`);
      });

      engine.act(fa.addPlayer('Bill', 'bb'));

      expect(
        engine.getStateRep('spec', {
          section: 'players',
        })
      ).toEqual(`Players (2)\n- aa John\n- bb Bill\n`);
    });
  });

  describe('createZone action', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine(createWithPlayers());
    });

    test('removedFromGame - no owner, accessible to all', () => {
      engine.act(fa.createZone('bunch', null, 'removedFromGame', 'all', null));

      expect(engine.getStateRep('spec', { section: 'zones' }))
        .toEqual(`Zones (1)
- removedFromGame ba all (0)
`);

      expect(engine.getLastDeltaRep('spec')).toEqual(
        ':zoneCreated removedFromGame ba all'
      );
      expect(engine.getLastDeltaRep('aa')).toEqual(
        ':zoneCreated removedFromGame ba all'
      );
    });

    test('hand - has owner, accessible to one person', () => {
      engine.act(fa.createZone('bunch', 'aa', 'hand', ['aa'], 'library'));

      expect(engine.getStateRep('spec', { section: 'zones' }))
        .toEqual(`Zones (1)
- aa:hand bi aa (0)
`);

      expect(engine.getLastDeltaRep('spec')).toEqual(
        ':zoneCreated aa:hand bi aa'
      );

      expect(engine.getLastDeltaRep('aa')).toEqual(
        ':zoneCreated aa:hand ba aa'
      );
    });

    test('uncontrolled - region, has owner, accessible to one', () => {
      engine.act(
        fa.createZone('region', 'aa', 'uncontrolled', ['aa'], 'crypt')
      );

      expect(engine.getStateRep('spec', { section: 'zones' }))
        .toEqual(`Zones (1)
- aa:uncontrolled ri aa (0)
`);

      expect(engine.getLastDeltaRep('spec')).toEqual(
        ':zoneCreated aa:uncontrolled ri aa'
      );
      expect(engine.getLastDeltaRep('aa')).toEqual(
        ':zoneCreated aa:uncontrolled ra aa'
      );
    });
  });

  describe('seq action', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine();
    });

    test('simple sequence', () => {
      engine.act(
        fa.seq(
          fa.addPlayer('John', 'aa'),
          fa.createZone('bunch', 'aa', 'hand', ['aa'], null)
        )
      );

      expect(engine.getLastDeltaRep('spec')).toStrictEqual(`:seq
- :playerAdded John aa
- :zoneCreated aa:hand bi aa`);
    });

    test('seq in seq - collapse one-delta seqs', () => {
      engine.act(
        fa.seq(
          fa.addPlayer('John', 'aa'),
          fa.seq(fa.createZone('bunch', 'aa', 'hand', ['aa'], null))
        )
      );

      expect(engine.getLastDeltaRep('spec')).toStrictEqual(`:seq
- :playerAdded John aa
- :zoneCreated aa:hand bi aa`);
    });
  });

  describe('createCard action - in bunch', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine(createWithZones());
    });

    test('in library - bunch unknown', () => {
      engine.act(fa.createCard('krc', 'library', 'aa:library'));

      expect(
        engine.getStateRep('spec', { section: 'zones', onlyZone: 'aa:library' })
      ).toEqual(`Zones
- aa:library bi none (1)
`);

      expect(engine.getLastDeltaRep('spec')).toStrictEqual(
        `:cardCreated aa:library@0 ?`
      );
      expect(engine.getLastDeltaRep('aa')).toStrictEqual(
        `:cardCreated aa:library@0 ?`
      );
    });

    test('in hand - bunch known to owner', () => {
      engine.act(fa.createCard('krc', 'library', 'aa:hand'));

      expect(
        engine.getStateRep('aa', { section: 'zones', onlyZone: 'aa:hand' })
      ).toEqual(`Zones
- aa:hand ba aa (1)
  - krc#1
`);

      expect(engine.getLastDeltaRep('spec')).toStrictEqual(
        `:cardCreated aa:hand@0 ?`
      );

      expect(engine.getLastDeltaRep('aa')).toStrictEqual(
        `:cardCreated aa:hand@0 krc#1`
      );
    });

    test('in ash heap - bunch known to all', () => {
      engine.act(fa.createCard('krc', 'library', 'aa:ashHeap'));

      expect(
        engine.getStateRep('spec', { section: 'zones', onlyZone: 'aa:ashHeap' })
      ).toEqual(`Zones
- aa:ashHeap ba all (1)
  - krc#1
`);

      expect(engine.getLastDeltaRep('spec')).toStrictEqual(
        `:cardCreated aa:ashHeap@0 krc#1`
      );

      expect(engine.getLastDeltaRep('aa')).toStrictEqual(
        `:cardCreated aa:ashHeap@0 krc#1`
      );

      expect(engine.getLastDeltaRep('bb')).toStrictEqual(
        `:cardCreated aa:ashHeap@0 krc#1`
      );
    });
  });

  describe('createCard action - in region', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine(createWithZones());
    });

    test('in ready, faceup - region known to all', () => {
      engine.act(fa.createCard('korah', 'crypt', 'aa:ready'));

      expect(
        engine.getStateRep('aa', { section: 'zones', onlyZone: 'aa:ready' })
      ).toEqual(`Zones
- aa:ready ra all (1)
  - korah#1 ur
`);
      expect(engine.getLastDeltaRep('spec')).toEqual(
        `:cardCreated aa:ready@0 korah#1 ur`
      );
    });

    test('in ready, facedown - region known to all', () => {
      engine.act(
        fa.createCard('korah', 'crypt', 'aa:ready', {
          facing: 'facedown',
        })
      );

      expect(
        engine.getStateRep('aa', { section: 'zones', onlyZone: 'aa:ready' })
      ).toEqual(`Zones
- aa:ready ra all (1)
  - korah#1 dr
`);

      expect(engine.getLastDeltaRep('spec')).toEqual(
        `:cardCreated aa:ready@0 korah#1 dr`
      );

      expect(engine.getLastDeltaRep('aa')).toEqual(
        `:cardCreated aa:ready@0 korah#1 dr`
      );
    });

    test('in uncontrolled, faceup - region known to one', () => {
      engine.act(
        // Note: it wouldn't happen in game, but it's just an example
        fa.createCard('korah', 'crypt', 'aa:uncontrolled', {
          facing: 'faceup',
        })
      );

      expect(
        engine.getStateRep('spec', {
          section: 'zones',
          onlyZone: 'aa:uncontrolled',
        })
      ).toEqual(`Zones
- aa:uncontrolled ri aa (1)
  - korah#1 ur
`);

      expect(engine.getLastDeltaRep('spec')).toEqual(
        `:cardCreated aa:uncontrolled@0 korah#1 ur`
      );

      expect(engine.getLastDeltaRep('aa')).toEqual(
        `:cardCreated aa:uncontrolled@0 korah#1 ur`
      );
    });

    test('in uncontrolled, facedown - region known to one', () => {
      engine.act(
        fa.createCard('korah', 'crypt', 'aa:uncontrolled', {
          facing: 'facedown',
        })
      );

      expect(
        engine.getStateRep('spec', {
          section: 'zones',
          onlyZone: 'aa:uncontrolled',
        })
      ).toEqual(`Zones
- aa:uncontrolled ri aa (1)
  - Haa#1 dr
`);
      expect(engine.getLastDeltaRep('spec')).toEqual(
        // Note: card owner is assumed from the initial zone of creation
        `:cardCreated aa:uncontrolled@0 Haa#1 dr`
      );

      expect(engine.getLastDeltaRep('aa')).toEqual(
        `:cardCreated aa:uncontrolled@0 korah#1 dr`
      );

      expect(engine.getLastDeltaRep('bb')).toEqual(
        `:cardCreated aa:uncontrolled@0 Haa#1 dr`
      );
    });
  });

  describe('changeCardFacing action', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine(
        getStateAfterActions(
          createWithZones(),
          fa.createCard('ashley', 'crypt', 'aa:uncontrolled', {
            facing: 'facedown',
          }),
          fa.createCard('colett', 'crypt', 'aa:uncontrolled', {
            facing: 'faceup',
          })
        )
      );
    });

    test('facedown->faceup', () => {
      engine.act(fa.changeCardFacing('ashley:1', 'faceup'));

      expect(
        engine.getStateRep('spec', {
          section: 'zones',
          onlyZone: 'aa:uncontrolled',
        })
      ).toEqual(`Zones
- aa:uncontrolled ri aa (2)
  - ashley#1 ur
  - colett#1 ur
`);

      expect(engine.getLastDeltaRep('spec')).toEqual(
        `:cardChangedFacing aa:uncontrolled@0 ashley#1 faceup`
      );

      expect(engine.getLastDeltaRep('aa')).toEqual(
        `:cardChangedFacing aa:uncontrolled@0 ashley#1 faceup`
      );
    });

    describe('facedown->faceup', () => {
      beforeEach(() => {
        engine.act(fa.changeCardFacing('colett:1', 'facedown'));
      });

      test('next state should have this card facedown', () => {
        expect(
          engine.getStateRep('spec', {
            section: 'zones',
            onlyZone: 'aa:uncontrolled',
          })
        ).toEqual(`Zones
- aa:uncontrolled ri aa (2)
  - Haa#1 dr
  - colett#1 dr
`);

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardChangedFacing aa:uncontrolled@1 colett#1 facedown`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardChangedFacing aa:uncontrolled@1 colett#1 facedown`
        );
      });

      describe('and ->faceup again', () => {
        beforeEach(() => {
          engine.act(fa.changeCardFacing('colett:1', 'faceup'));
        });

        test('delta for non-owner: from past to full without new cid', () => {
          expect(engine.getLastDeltaRep('spec')).toEqual(
            `:cardChangedFacing aa:uncontrolled@1 colett#1 faceup`
          );
        });
      });
    });
  });

  describe('moveCard action', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine(createWithZones());
    });

    describe('from bunch to bunch', () => {
      beforeEach(() => {
        engine.act(fa.createCard('gothun', 'library', 'aa:library'));
        engine.act(fa.moveCard('gothun:1', 'aa:hand'));
      });

      test('state and delta reps', () => {
        expect(
          engine.getStateRep('aa', {
            section: 'zones',
            onlyZone: 'aa:hand',
          })
        ).toEqual(`Zones
- aa:hand ba aa (1)
  - gothun#1
`);

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:library@0 aa:hand@0 ?`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:library@0 aa:hand@0 gothun#1`
        );
      });

      describe('back to bunch', () => {
        beforeEach(() => {
          engine.act(fa.moveCard('gothun:1', 'aa:library'));
        });

        test('rep for accessible -> inaccessible', () => {
          expect(engine.getLastDeltaRep('aa')).toEqual(
            `:cardMoved aa:hand@0 aa:library@0 ?`
          );
        });
      });

      describe('to another accessible bunch', () => {
        beforeEach(() => {
          engine.act(fa.moveCard('gothun:1', 'aa:ashHeap'));
        });

        test('rep for accessible -> accessible', () => {
          expect(engine.getLastDeltaRep('aa')).toEqual(
            `:cardMoved aa:hand@0 aa:ashHeap@0 gothun#1`
          );
        });
      });
    });

    describe('from bunch to region', () => {
      beforeEach(() => {
        engine.act(
          fa.seq(
            fa.createCard('ashley', 'crypt', 'aa:crypt'),
            fa.createCard('colett', 'crypt', 'aa:crypt'),
            fa.createCard('meaghan', 'crypt', 'aa:ashHeap'),
            fa.createCard('sully', 'crypt', 'aa:ashHeap')
          )
        );
      });

      test('promote; rep inacc -> fd inacc; rep inacc -> fd acc', () => {
        engine.act(
          fa.moveCard('ashley:1', 'aa:uncontrolled', {
            facing: 'facedown',
          })
        );

        expect(
          engine.getStateRep('aa', {
            section: 'zones',
            onlyZone: 'aa:uncontrolled',
          })
        ).toEqual(`Zones
- aa:uncontrolled ra aa (1)
  - ashley#1 dr
`);

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:crypt@0 aa:uncontrolled@0 Haa#1 dr`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:crypt@0 aa:uncontrolled@0 ashley#1 dr`
        );
      });

      test('inacc -> fu inacc; inacc -> fu acc', () => {
        engine.act(
          // cannot happen usually
          fa.moveCard('colett:1', 'aa:uncontrolled', {
            facing: 'faceup',
          })
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:crypt@1 aa:uncontrolled@0 colett#1 ur`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:crypt@1 aa:uncontrolled@0 colett#1 ur`
        );
      });

      test('rep for acc -> fd inacc; acc -> fd acc', () => {
        engine.act(
          // cannot happen usually
          fa.moveCard('meaghan:1', 'aa:uncontrolled', {
            facing: 'facedown',
          })
        );

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:ashHeap@0 aa:uncontrolled@0 meaghan#1 dr`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:ashHeap@0 aa:uncontrolled@0 meaghan#1 dr`
        );
      });

      test('rep for acc -> fu acc', () => {
        engine.act(
          // cannot happen usually
          fa.moveCard('sully:1', 'aa:uncontrolled', {
            facing: 'faceup',
          })
        );

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:ashHeap@1 aa:uncontrolled@0 sully#1 ur`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:ashHeap@1 aa:uncontrolled@0 sully#1 ur`
        );
      });
    });

    describe('from region facedown to bunch', () => {
      beforeEach(() => {
        engine.act(
          fa.seq(
            fa.createCard('ashley', 'crypt', 'aa:uncontrolled', {
              facing: 'facedown',
            }),
            fa.createCard('colett', 'crypt', 'aa:uncontrolled', {
              facing: 'facedown',
            })
          )
        );
      });

      test('demote; rep for inacc -> inacc; acc -> inacc', () => {
        engine.act(fa.moveCard('ashley:1', 'aa:crypt'));

        expect(
          engine.getStateRep('aa', {
            section: 'zones',
            onlyZone: 'aa:crypt',
          })
        ).toEqual(`Zones
- aa:crypt bi none (1)
`);

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:uncontrolled@0 aa:crypt@0 ?`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:uncontrolled@0 aa:crypt@0 ?`
        );
      });

      test('rep for inacc -> acc; acc -> acc', () => {
        engine.act(fa.moveCard('colett:1', 'aa:ashHeap'));

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:uncontrolled@1 aa:ashHeap@0 colett#1`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:uncontrolled@1 aa:ashHeap@0 colett#1`
        );
      });
    });

    describe('from region faceup to bunch', () => {
      beforeEach(() => {
        engine.act(
          fa.seq(
            fa.createCard('ashley', 'crypt', 'aa:uncontrolled', {
              facing: 'faceup',
            }),
            fa.createCard('colett', 'crypt', 'aa:uncontrolled', {
              facing: 'faceup',
            })
          )
        );
      });

      test('demote; inacc -> inacc; acc -> inacc', () => {
        engine.act(fa.moveCard('ashley:1', 'aa:crypt'));

        expect(
          engine.getStateRep('aa', {
            section: 'zones',
            onlyZone: 'aa:crypt',
          })
        ).toEqual(`Zones
- aa:crypt bi none (1)
`);

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:uncontrolled@0 aa:crypt@0 ?`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:uncontrolled@0 aa:crypt@0 ?`
        );
      });

      test('rep for inacc -> acc; acc -> acc', () => {
        engine.act(fa.moveCard('colett:1', 'aa:ashHeap'));

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:uncontrolled@1 aa:ashHeap@0 colett#1`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:uncontrolled@1 aa:ashHeap@0 colett#1`
        );
      });
    });

    describe('from region facedown to region', () => {
      beforeEach(() => {
        engine.act(
          fa.seq(
            fa.createCard('ashley', 'crypt', 'aa:uncontrolled', {
              facing: 'facedown',
            }),
            fa.createCard('colett', 'crypt', 'aa:uncontrolled', {
              facing: 'facedown',
            })
          )
        );
      });

      test('have it in the target zone; inacc -> inacc; acc -> inac', () => {
        engine.act(fa.moveCard('ashley:1', 'bb:uncontrolled'));

        expect(
          engine.getStateRep('bb', {
            section: 'zones',
            onlyZone: 'bb:uncontrolled',
          })
        ).toEqual(`Zones
- bb:uncontrolled ra bb (1)
  - ashley#1 dr
`);

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:uncontrolled@0 bb:uncontrolled@0 Haa#1 dr`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:uncontrolled@0 bb:uncontrolled@0 ashley#1 dr`
        );
      });

      test('inaccessible -> accessible; acc -> acc', () => {
        engine.act(fa.moveCard('colett:1', 'aa:ready'));

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:uncontrolled@1 aa:ready@0 colett#1 dr`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:uncontrolled@1 aa:ready@0 colett#1 dr`
        );
      });
    });

    describe('from region faceup to region', () => {
      beforeEach(() => {
        engine.act(
          fa.seq(
            fa.createCard('ashley', 'crypt', 'aa:uncontrolled', {
              facing: 'faceup',
            }),
            fa.createCard('colett', 'crypt', 'aa:uncontrolled', {
              facing: 'faceup',
            })
          )
        );
      });

      test('have it in the target zone; inacc -> inacc; acc -> inacc', () => {
        engine.act(fa.moveCard('ashley:1', 'bb:uncontrolled'));

        expect(
          engine.getStateRep('bb', {
            section: 'zones',
            onlyZone: 'bb:uncontrolled',
          })
        ).toEqual(`Zones
- bb:uncontrolled ra bb (1)
  - ashley#1 ur
`);

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:uncontrolled@0 bb:uncontrolled@0 ashley#1 ur`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:uncontrolled@0 bb:uncontrolled@0 ashley#1 ur`
        );
      });

      test('inacc -> acc; acc -> acc', () => {
        engine.act(fa.moveCard('colett:1', 'aa:ready'));

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:uncontrolled@1 aa:ready@0 colett#1 ur`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:uncontrolled@1 aa:ready@0 colett#1 ur`
        );
      });
    });
  });

  describe('changeCardRotation action', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine(createWithZones());
      engine.act(fa.createCard('ashley', 'crypt', 'aa:ready'));
    });

    describe('to locked', () => {
      beforeEach(() => {
        engine.act(fa.changeCardRotation('ashley:1', 'locked'));
      });
      test('should set the rotation', () => {
        expect(
          engine.getStateRep('aa', { section: 'zones', onlyZone: 'aa:ready' })
        ).toEqual(`Zones
- aa:ready ra all (1)
  - ashley#1 ul
`);

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardChangedRotation aa:ready@0 ashley#1 locked`
        );
      });

      describe('and back again', () => {
        test('should set the rotation back to ready', () => {
          engine.act(fa.changeCardRotation('ashley:1', 'ready'));

          expect(
            engine.getStateRep('aa', { section: 'zones', onlyZone: 'aa:ready' })
          ).toEqual(`Zones
- aa:ready ra all (1)
  - ashley#1 ur
`);

          expect(engine.getLastDeltaRep('spec')).toEqual(
            `:cardChangedRotation aa:ready@0 ashley#1 ready`
          );
        });
      });
    });
  });

  describe('attachCard action', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine(createWithZones());
      engine.act(
        fa.seq(
          fa.createCard('ashley', 'crypt', 'aa:ready'),
          fa.createCard('colett', 'crypt', 'aa:ready'),
          // In an actual game, it would go through the current zone, but for testing
          // inaccessible->attachment we do it from hand.
          fa.createCard('magnum', 'library', 'aa:hand')
        )
      );
    });

    describe('attach', () => {
      beforeEach(() => {
        engine.act(fa.attachCard('magnum:1', 'ashley:1'));
      });

      test('attach promoted; inacc -> acc; acc -> acc', () => {
        expect(
          engine.getStateRep('aa', { section: 'zones', onlyZone: 'aa:ready' })
        ).toEqual(`Zones
- aa:ready ra all (2)
  - ashley#1 ur
    - magnum#1 ur
  - colett#1 ur
`);

        expect(engine.getLastDeltaRep('spec')).toEqual(
          `:cardMoved aa:hand@0 aa:ready@0@0 magnum#1 ur`
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardMoved aa:hand@0 aa:ready@0@0 magnum#1 ur`
        );
      });

      describe('attachCard when starts out attached', () => {
        test('should move the card from previous place', () => {
          engine.act(fa.attachCard('magnum:1', 'colett:1'));

          expect(
            engine.getStateRep('aa', { section: 'zones', onlyZone: 'aa:ready' })
          ).toEqual(`Zones
- aa:ready ra all (2)
  - ashley#1 ur
  - colett#1 ur
    - magnum#1 ur
`);

          expect(engine.getLastDeltaRep('spec')).toEqual(
            `:cardMoved aa:ready@0@0 aa:ready@1@0 magnum#1 ur`
          );
        });
      });

      describe('moveCard when starts out attached', () => {
        beforeEach(() => {
          engine.act(fa.moveCard('magnum:1', 'aa:ashHeap'));
        });

        test('should move the card from previous place', () => {
          expect(
            engine.getStateRep('aa', {
              section: 'zones',
              onlyZones: ['aa:ready', 'aa:ashHeap'],
            })
          ).toEqual(`Zones
- aa:ready ra all (2)
  - ashley#1 ur
  - colett#1 ur
- aa:ashHeap ba all (1)
  - magnum#1
`);

          expect(engine.getLastDeltaRep('aa')).toEqual(
            `:cardMoved aa:ready@0@0 aa:ashHeap@0 magnum#1`
          );
        });
      });

      test('changeCardFacing when card is attached', () => {
        engine.act(fa.changeCardFacing('magnum:1', 'facedown'));

        expect(
          engine.getStateRep('aa', {
            section: 'zones',
            onlyZones: ['aa:ready'],
          })
        ).toEqual(`Zones
- aa:ready ra all (2)
  - ashley#1 ur
    - magnum#1 dr
  - colett#1 ur
`);

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardChangedFacing aa:ready@0@0 magnum#1 facedown`
        );
      });

      test('changeCardRotation when card is attached', () => {
        engine.act(fa.changeCardRotation('magnum:1', 'locked'));

        expect(
          engine.getStateRep('aa', {
            section: 'zones',
            onlyZones: ['aa:ready'],
          })
        ).toEqual(`Zones
- aa:ready ra all (2)
  - ashley#1 ur
    - magnum#1 ul
  - colett#1 ur
`);

        expect(engine.getLastDeltaRep('aa')).toEqual(
          `:cardChangedRotation aa:ready@0@0 magnum#1 locked`
        );
      });
    });
  });

  describe('setCardVars action', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine(createWithZones());
    });

    test('should set the vars', () => {
      engine.act(fa.createCard('ashley', 'crypt', 'aa:ready'));

      engine.act(fa.setCardVars('ashley:1', { bleed: 1, blood: 3 }));

      expect(
        engine.getStateRep('aa', {
          section: 'zones',
          onlyZone: 'aa:ready',
        })
      ).toEqual(`Zones
- aa:ready ra all (1)
  - ashley#1 ur bleed=1,blood=3
`);

      expect(engine.getLastDeltaRep('aa')).toEqual(
        `:cardVarsSet aa:ready@0 ashley#1 bleed=1,blood=3`
      );
    });
  });

  describe('changeCardVars action', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine(createWithZones());
      engine.act(
        fa.seq(
          fa.createCard('ashley', 'crypt', 'aa:ready'),
          fa.setCardVars('ashley:1', { blood: 3, stealth: 0 })
        )
      );
    });

    test('should modify the vars', () => {
      engine.act(fa.changeCardVars('ashley:1', { blood: -1, stealth: 1 }));

      expect(
        engine.getStateRep('aa', {
          section: 'zones',
          onlyZone: 'aa:ready',
        })
      ).toEqual(`Zones
- aa:ready ra all (1)
  - ashley#1 ur blood=2,stealth=1
`);

      expect(engine.getLastDeltaRep('aa')).toEqual(
        `:cardVarsChanged aa:ready@0 ashley#1 blood-1=2,stealth+1=1`
      );
    });
  });

  describe('setVars action', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine();
    });

    test('should set the vars', () => {
      engine.act(fa.setVars({ turn: 1, activePlayerRole: 'bb' }));

      expect(engine.getStateRep('spec', { section: 'vars' })).toEqual(`Vars
- activePlayerRole=bb
- turn=1
`);

      expect(engine.getLastDeltaRep('spec')).toEqual(
        `:stateVarsSet activePlayerRole=bb,turn=1`
      );
    });
  });

  describe('changeVars action', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine(createWithPlayers());
    });

    test('should change the vars', () => {
      engine.act(fa.changeVars({ 'aa:pool': -3 }));

      expect(engine.getStateRep('spec', { section: 'vars' })).toEqual(`Vars
- aa:pool=27
- aa:vp=0
- activePlayerRole=aa
- bb:pool=30
- bb:vp=0
- edgeRole=null
- phase=unlock
- round=1
`);

      expect(engine.getLastDeltaRep('spec')).toEqual(
        `:stateVarsChanged aa:pool-3=27`
      );
    });
  });

  describe('setZoneAccess action', () => {
    let engine;

    beforeEach(() => {
      engine = new TestEngine(createWithZones());
      engine.act(
        fa.createCard('gothun', 'library', 'aa:library'),
        fa.createCard('gothun', 'library', 'aa:library'),
        fa.createCard('gothun', 'library', 'aa:library')
      );
    });

    test('set the access; inacc -> inacc; inacc -> acc', () => {
      engine.act(fa.setZoneAccess('aa:library', ['aa']));

      expect(
        engine.getStateRep('aa', { section: 'zones', onlyZone: 'aa:library' })
      ).toEqual(`Zones
- aa:library ba aa (3)
  - gothun#1
  - gothun#2
  - gothun#3
`);

      expect(engine.getLastDeltaRep('spec')).toEqual(
        `:zoneAccessSet aa:library aa inaccessible`
      );

      expect(engine.getLastDeltaRep('aa'))
        .toEqual(`:zoneAccessSet aa:library aa accessible
- gothun#1
- gothun#2
- gothun#3`);
    });

    describe('when inside a seq', () => {
      test('should format indentation correctly', () => {
        engine.act(
          fa.seq(
            fa.setVars({ 'aa:pool': 32 }),
            fa.setZoneAccess('aa:library', ['aa'])
          )
        );

        expect(engine.getLastDeltaRep('aa')).toEqual(`:seq
- :stateVarsSet aa:pool=32
- :zoneAccessSet aa:library aa accessible
  - gothun#1
  - gothun#2
  - gothun#3`);
      });
    });
  });
});
