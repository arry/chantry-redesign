// TODO nominal type to distinguish from other strings
export type Role = string;

export interface VDelta<T = any> {
  deltaType: T;
  [extraProps: string]: any;
}

export interface Delta<T = any> {
  deltaType: T;
  toView: (pov: Role) => { _replace?: any; [k: string]: any };
  [extraProps: string]: any;
}

export interface DeltaCreator<D> {
  (...args: any[]): D;
}

export interface DeltaCreatorsMapObject<D extends Delta> {
  [key: string]: DeltaCreator<D>;
}

// utils for base language types
export type Hash<K extends string | number | symbol, V> = Partial<Record<K, V>>;

export type StringHash<V> = {
  [k: string]: V;
};

// building blocks

export type RoleToPlayerName = Hash<Role, string>;

// info about cards

export type CardTag = 'cardInBunch' | 'cardInRegion';

export type CardFacing = 'faceup' | 'facedown';

export type CardRotation = 'ready' | 'locked';

// Note: it could have been an enumeration 'crypt' | 'library', but for
// generality with other games let it be any old string.
export type CardBack = string;

export type BasicVarValue = string | number | boolean | null;

export type VarValue = BasicVarValue | BasicVarValue[];

export type VarValues = {
  [varName: string]: VarValue;
};

export type VarChangers = {
  [varName: string]: number;
};

export type Vars = StringHash<VarValue>;

// card knowledge

export type FullCardKnowledge = {
  knowledgeType: 'full';
  cid: string;
};

export type LimitedCardKnowledge = {
  knowledgeType: 'limited';
  cid: string;
};

export type CardKnowledge = null | FullCardKnowledge | LimitedCardKnowledge;

// card registry

export type CardRegistry = {
  role: Role;
  baseToCounter: StringHash<number>;
  cidToSid: StringHash<string>;
};

// Card

interface CardCommon {
  sid: string;
  owner: Role;
  cardBack: CardBack;
  roleToKnowledge: Hash<Role, CardKnowledge>;
}

export interface CardInBunch extends CardCommon {
  tag: 'cardInBunch';
}

export interface CardInRegion extends CardCommon {
  tag: 'cardInRegion';
  facing: CardFacing;
  rotation: CardRotation;
  vars: Vars;
  attachments: Region;
}

export type Card = CardInBunch | CardInRegion;

// Zone

export type ZoneAccess = 'all' | Role[];

export type ZoneKind = 'bunch' | 'region';

export type ZoneName = string;

export interface Zone {
  zoneKind: ZoneKind;
  owner: Role | null;
  slug: string;
  // For regions, access means "who can look at the facedown cards", because
  // the regions are assumed to be visible to all.  This excludes games where
  // we can manipulate card zones behind a screen, of which I know none.
  access: ZoneAccess;
  cards: Card[];
}

export interface Bunch extends Zone {
  zoneKind: 'bunch';
  cards: CardInBunch[];
}

export interface Region extends Zone {
  zoneKind: 'region';
  cards: CardInRegion[];
}

// history

export type LogLineArgDescriptor =
  | number
  | string
  | {
      value: any;
    }
  | { fromStash: string };

export type LogLineArgsDescriptor = {
  [argName: string]: LogLineArgDescriptor;
};

export type LogLine = {
  templateCode: string;
  args: {
    // a: array of strings; c: card; etc.
    [argName: string]: any;
  };
};

export type HistoryEntry = {
  createdAt: number; // Date.getTime
  entryType: 'logLine';
  logLine: LogLine;
};

// State

export type Outcome = {
  mode: string;
  submode: string;
  winnerRole: Role;
};

export type State = {
  code: string;
  roleToPlayerName: RoleToPlayerName;
  roleToCardRegistry: Hash<Role, CardRegistry>;
  roles: Role[];
  zones: Hash<ZoneName, Zone>;
  vars: Vars;
  baseToCounter: StringHash<number>;
  historyEntries: HistoryEntry[];
  stash: StringHash<unknown>;
  outcome: Outcome | null;
};

// orders

export type OrderSourceCard = { sourceType: 'card'; card: Card };
export type OrderSourceZone = { sourceType: 'zone'; zoneName: string };
export type OrderSourcePlayer = { sourceType: 'player'; role: Role };

export type OrderSource =
  | 'general'
  | OrderSourceCard
  | OrderSourceZone
  | OrderSourcePlayer;

export type VOrderSource = string;

export type NextLevelOrder = {
  orderType: string;
  source: OrderSource;
  orderName: string;
  additionalData?: StringHash<any>;
};

export type VNextLevelOrder = {
  orderType: string;
  orderName: string;
  source: VOrderSource;
};

export type OrderConversion = {
  key: string;
  out: string;
  conversionType: 'cardsToCids';
};

export type OrderWithImpliedName = {
  role: Role;
  orderType: string;
  source: OrderSource;
  additionalData?: StringHash<any>;
  isDisabled?: boolean;
  isRepeatable?: boolean;
  orderRow?: string;
  nextLevelOrders?: NextLevelOrder[];
  conversions?: OrderConversion[];
};

export type Order = OrderWithImpliedName & {
  orderName: string;
  disambiguationNumber: number;
};

export type VOrder = {
  orderType: string;
  orderName: string;
  source: VOrderSource;
  disambiguationNumber: number;
  isDisabled?: boolean;
  isRepeatable?: boolean;
  orderRow?: string;
  nextLevelOrders?: VNextLevelOrder[];
  additionalData?: StringHash<any>;
};

export type FilledOrder = any; // TODO

export type OrderDescriptor = {
  collect(
    state: State
  ): OrderWithImpliedName | (OrderWithImpliedName | null)[] | null;
  execute(state: State, order: Order, filledOrder: FilledOrder): AAResult;
};

export type OrderDescriptorsMapObject = StringHash<OrderDescriptor>;

export type OrderDescriptorGroupOptions = {
  precondition?: (state: State) => boolean;
};

export type OrderDescriptorGroup = {
  code: string;
  name: string;
  options: OrderDescriptorGroupOptions;
  descriptors: OrderDescriptorsMapObject;
};

export type OrderDescriptorGroupsMapObject = StringHash<OrderDescriptorGroup>;

export type VFilledOrder = any; // TODO

// VState

export type VYourAccess = 'accessible' | 'inaccessible';

export type VVars = Vars;

export type VCard = {
  tag: CardTag;
  yourKnowledge: CardKnowledge;
  facing?: CardFacing;
  rotation?: CardRotation;
  vars?: VVars;
  attachedCards?: VCard[];
  cardBack: CardBack;
};

export type VZone = {
  zoneKind: ZoneKind;
  owner: Role | null;
  slug: string;
  access: ZoneAccess;
  yourAccess: VYourAccess;
  cards: VCard[] | null;
  nCards: number;
  topCardBack: CardBack | null;
};

export type VZones = {
  [zoneName: string]: VZone;
};

export type VState = {
  pov: Role;
  roleToPlayerName: RoleToPlayerName;
  roles: Role[];
  zones: VZones;
  vars: VVars;
  historyEntries: HistoryEntry[];
  outcome: Outcome | null;
};

// Actions and application

export type AAResult = {
  next: State;
  delta: Delta | null;
};

export interface Action<T = any> {
  actionType: T;
  args: any[];
}

export interface AnyAction extends Action {}

export type ActionApplicator<A extends Action = AnyAction> = (
  state: State,
  ...args: any[]
) => AAResult;

export interface ActionApplicatorsMapObject<A extends Action = AnyAction> {
  [key: string]: ActionApplicator<A>;
}

// Utilities for actions

export type CardLocation = {
  zoneName: ZoneName;
  index: number;
  attachmentIndex?: number;
};

export type Context = {
  state: State;
};

// Options for actions

export type CreateCardOptions = {
  facing?: CardFacing | null;
  rotation?: CardRotation | null;
};

export type MoveCardOptions = {
  sourceLocation?: CardLocation | null;
  facing?: CardFacing | null;
  rotation?: CardRotation | null;
  saveAs?: string | null;
  saveTargetLocationAs?: string | null;
};

export type InitialAccess = 'none' | 'all' | 'owner';
