import type { State, Delta } from '../types';
import { createEmptyState } from '../creation';
import { applyAction } from '../actionsInfrastructure';
import { stateToRep } from '../rep';
import { deltaToRep } from '../deltas';
import { fa } from '../foundationActions';

export class TestEngine {
  state: State;
  deltas: Delta[];

  constructor(state: State = null) {
    this.state = state ? state : createEmptyState('mv');
    this.deltas = [];
  }

  act(...actions) {
    const { next, delta } = applyAction(this.state, fa.seq(...actions));
    this.state = next;
    this.deltas.push(delta);
  }

  getLastDeltaRep(pov = 'spec') {
    const delta = this.deltas[this.deltas.length - 1];
    return deltaToRep(delta, pov);
  }

  getStateRep(pov = 'spec', options = {}) {
    return stateToRep(this.state, pov, options);
  }
}
