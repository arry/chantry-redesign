import { mvZoneDescriptors } from '../mv/mvZoneDescriptors';
import { ZoneDescriptors } from '../creation';
import { createMvState } from '../mv/mvState';

export const players = [{ playerName: 'John' }, { playerName: 'Bill' }];

export const smallZoneDescriptors: ZoneDescriptors = {
  library: {
    zoneKind: 'bunch',
    initAccess: 'none',
  },
  hand: {
    zoneKind: 'bunch',
    initAccess: 'owner',
  },
  uncontrolled: {
    zoneKind: 'region',
    initAccess: 'owner',
  },
  support: {
    zoneKind: 'region',
    initAccess: 'all',
  },
};

export function createWithPlayers() {
  return createMvState({ players });
}

export function createWithZones() {
  return createMvState({
    players,
    zones: mvZoneDescriptors,
  });
}
