import { createEmptyState } from './creation';
import { stateToVState } from './view';
import { dc, deltaToVDelta, deltaToRep, applyVDelta } from './deltas';

describe('empty delta', () => {
  const delta = dc.empty();

  test('rep', () => {
    expect(deltaToRep(delta, 'spec')).toBe(':empty');
  });

  test('applying', () => {
    const state = createEmptyState('mv');
    const pov = 'spec';
    const vState = stateToVState(state, pov);
    const vDelta = deltaToVDelta(delta, pov);

    expect(applyVDelta(vState, vDelta)).toStrictEqual(vState);
  });
});
