import type {
  RoleToPlayerName,
  Role,
  ZoneKind,
  ZoneAccess,
  CardFacing,
  CardRotation,
  VCard,
  VYourAccess,
  VVars,
  VZone,
  VZones,
  VState,
  State,
  CardLocation,
  VarValues,
  VarChangers,
} from './types';
import {
  entriesSortedAlphabetically,
  getTranslatedCidFromKnowledge,
  indentedList,
  getZoneYourAccess,
  entryToValue,
  qualifiedName,
} from './utils';
import { stateToVState } from './view';

export type StateToRepOptions = {
  section?: string;
  onlyZone?: string;
  onlyZones?: string[];
};

export function getPlayersSectionRep(
  roleToPlayerName: RoleToPlayerName,
  roles: Role[]
): string {
  const header = `Players (${roles.length})`;
  const lines = roles.map((role) => `- ${role} ${roleToPlayerName[role]}`);
  return [header, ...lines].join('\n');
}

export function getZoneRepModifiers(
  zoneKind: ZoneKind,
  yourAccess: VYourAccess
): string {
  const zoneKindRep = zoneKind === 'bunch' ? 'b' : 'r';
  const yourAccessRep = yourAccess === 'accessible' ? 'a' : 'i';
  return `${zoneKindRep}${yourAccessRep}`;
}

export function getCardRepModifiers(
  facing: CardFacing,
  rotation: CardRotation
): string {
  const facingRep = facing === 'faceup' ? 'u' : 'd';
  const rotationRep = rotation === 'ready' ? 'r' : 'l';
  return `${facingRep}${rotationRep}`;
}

export function getSeparateVarReps(vars: VVars): string[] {
  return entriesSortedAlphabetically(vars).map(([varName, value]) => {
    return `${varName}=${value}`;
  });
}

export function getCardVarsRep(vars: VVars): string {
  const result = getSeparateVarReps(vars).join(',');
  return result;
}

export function vCardToRep(card: VCard, level = 0): string {
  const { yourKnowledge } = card;

  const translatedCid = getTranslatedCidFromKnowledge(yourKnowledge);
  let parts = [];
  if (card.tag === 'cardInBunch') {
    parts = [translatedCid];
  } else {
    const { facing, rotation, vars } = card;
    const modifiers = getCardRepModifiers(facing, rotation);
    const varsRep = getCardVarsRep(vars);
    parts = [translatedCid, modifiers, varsRep];
  }

  const head = parts.filter((part) => part.length > 0).join(' ');

  let items = [];

  if (card.tag === 'cardInRegion' && card.attachedCards.length > 0) {
    items = card.attachedCards.map((att) => vCardToRep(att, level + 1));
  }

  return indentedList(head, items, level + 1);
}

export function vZoneToRep(zone: VZone, level = 0, pov: Role): string {
  const { zoneKind, access, cards, nCards, owner, slug } = zone;

  const yourAccess = getZoneYourAccess(access, pov);

  const zoneName = owner ? qualifiedName(owner, slug) : slug;

  const modifiers = getZoneRepModifiers(zoneKind, yourAccess);
  const accessRep = getZoneAccessRep(access);

  const head = `${zoneName} ${modifiers} ${accessRep} (${nCards})`;

  const items =
    zone.cards !== null ? cards.map((card) => vCardToRep(card, level + 1)) : [];

  return indentedList(head, items, level + 1);
}

export function getZonesSectionRep(
  zones: VZones,
  pov: Role,
  options: StateToRepOptions = {}
): string {
  if (options.onlyZone) {
    const zoneName = options.onlyZone;
    const header = `Zones`;
    const zone = zones[zoneName];
    const rep = vZoneToRep(zone, 0, pov);
    return indentedList(header, [rep], 0);
  }

  let zoneReps, header;

  if (options.onlyZones) {
    zoneReps = options.onlyZones.map((zoneName) => {
      return vZoneToRep(zones[zoneName], 0, pov);
    });
    header = 'Zones';
  } else {
    zoneReps = entriesSortedAlphabetically(zones)
      .map(entryToValue)
      .map((zone) => vZoneToRep(zone, 0, pov));
    header = `Zones (${zoneReps.length})`;
  }

  return indentedList(header, zoneReps, 0);
}

export function getStateVarsSectionRep(vars: VVars): string {
  const reps = getSeparateVarReps(vars);
  return indentedList('Vars', reps, 0);
}

export function vStateToRep(
  vState: VState,
  options: StateToRepOptions = {}
): string {
  const includeSections = {
    you: true,
    players: true,
    vars: true,
    zones: true,
  };

  const sectionNames = Object.keys(includeSections);

  if (options.section) {
    for (const sectionName of sectionNames) {
      includeSections[sectionName] = sectionName === options.section;
    }
  }

  const sections = [];

  for (const sectionName of sectionNames) {
    if (!includeSections[sectionName]) {
      continue;
    }

    let section;
    if (sectionName === 'you') {
      const { pov } = vState;
      section = `You: ${pov}`;
    } else if (sectionName === 'players') {
      const { roleToPlayerName, roles } = vState;
      section = getPlayersSectionRep(roleToPlayerName, roles);
    } else if (sectionName === 'vars') {
      const { vars } = vState;
      section = getStateVarsSectionRep(vars);
    } else if (sectionName === 'zones') {
      const { pov, zones } = vState;
      section = getZonesSectionRep(zones, pov, options);
    } else {
      throw new Error(`unrecognized sectionName ${sectionName}`);
    }

    sections.push(section);
  }

  return sections.join('\n\n') + '\n';
}

export function stateToRep(
  state: State,
  pov: Role,
  options?: StateToRepOptions
): string {
  const vState = stateToVState(state, pov);
  const rep = vStateToRep(vState, options);
  return rep;
}

export function getCardLocationRep(location: CardLocation): string {
  const { zoneName, index, attachmentIndex } = location;
  const parts = [zoneName, index];
  if (attachmentIndex !== undefined) {
    parts.push(attachmentIndex);
  }
  return parts.join('@');
}

export function getSettersRep(setters: VarValues): string {
  const result = entriesSortedAlphabetically(setters)
    .map(([key, value]) => {
      return `${key}=${value}`;
    })
    .join(',');
  return result;
}

export function getChangersRep(
  changers: VarChangers,
  newValues: VarValues
): string {
  const result = entriesSortedAlphabetically(changers)
    .map(([varName, changer]) => {
      const upd = Number.isInteger(changer)
        ? changer < 0
          ? changer
          : `+${changer}`
        : changer;
      const value = newValues[varName];
      return `${varName}${upd}=${value}`;
    })
    .join(',');
  return result;
}

export function getZoneAccessRep(access: ZoneAccess): string {
  if (access === 'all') {
    return 'all';
  }
  if (access.length === 0) {
    return 'none';
  }
  return [...access].sort().join(',');
}
