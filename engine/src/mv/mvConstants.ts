export const mvDefaultMinionVarValues = {
  stealth: 0,
  intercept: 0,
  strength: 1,
  bleed: 1,
  votes: 0,
};

export const mvDefaultCardVarValues = {
  blood: 0,
  ...mvDefaultMinionVarValues,
};

export const havingBloodSlugs = [
  // Actually support cards don't, but they have generic counters sometimes
  // (e.g. Visit from the Capuchin), and for now we represent them with blood.
  // For the future, we should have distinct counter types for them.
  'support',
  'ready',
  'torpor',
  'uncontrolled',
];

export const inPlaySlugs = ['support', 'ready', 'torpor'];

export const minionSlugs = ['ready', 'torpor'];
