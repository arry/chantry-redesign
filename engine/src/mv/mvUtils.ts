import { Role, State } from '../types';
import { getStateVar, qualifiedName } from '../utils';

export function getActivePlayerRole(state): Role {
  return getStateVar(state, 'activePlayerRole') as Role;
}

export function getPlayingRoles(state: State): Role[] {
  return state.roles.filter(
    (role) => !getStateVar(state, qualifiedName('ousted', role), 0)
  );
}

export function getNonActiveRoles(state: State): Role[] {
  const activePlayerRole = getActivePlayerRole(state);
  return getPlayingRoles(state).filter((role) => role !== activePlayerRole);
}
