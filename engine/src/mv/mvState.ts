import type { StartGameDescriptor } from '../creation';
import { createEmptyState } from '../creation';
import type { State, Role } from '../types';
import { fa, getStateAfterActions } from '../foundationActions';
import './mvOrders'; // to imperatively register
import { mvBaseToCardInfo } from './mvCards';
import { EError } from '../errors';
import { buildZoneName, qualifiedName, initAccessToAccess } from '../utils';

const allPlayingRoles: Role[] = ['aa', 'bb', 'cc', 'dd', 'ee', 'ff'];

export function createMvState(options: StartGameDescriptor): State {
  const { players, zones = {}, skipInitialVars = false } = options;
  const roles = allPlayingRoles.slice(0, players.length);

  const addPlayerActions = [];

  const createCardActions = [];

  const unrecognizedBases = [];

  players.forEach(({ playerName, deck }, index) => {
    const role = roles[index];
    addPlayerActions.push(fa.addPlayer(playerName, role));

    if (deck) {
      ['crypt', 'library'].forEach((slug) => {
        (deck[slug] || []).forEach(({ base, count }) => {
          const cardInfo = mvBaseToCardInfo[base];
          if (!cardInfo) {
            unrecognizedBases.push(base);
          } else {
            const cardBack = cardInfo.back;
            for (let i = 0; i < count; ++i) {
              createCardActions.push(
                fa.createCard(base, cardBack, buildZoneName(role, slug))
              );
            }
          }
        });
      });
    }
  });

  if (unrecognizedBases.length > 0) {
    throw new EError(
      'Cannot create mv state because of unrecognized bases',
      unrecognizedBases
    );
  }

  const createZoneActions = [];

  for (const [slug, zoneDescriptor] of Object.entries(zones)) {
    const { zoneKind, initAccess } = zoneDescriptor;
    for (const owner of roles) {
      const access = initAccessToAccess(initAccess, owner);
      createZoneActions.push(fa.createZone(zoneKind, owner, slug, access));
    }
  }

  const setVarActions = skipInitialVars
    ? []
    : [
        fa.setVars({
          activePlayerRole: 'aa',
          edgeRole: null,
          round: 1,
          phase: 'unlock',
        }),
        ...roles.map((role) => {
          return fa.setVars({
            [qualifiedName(role, 'pool')]: 30,
            [qualifiedName(role, 'vp')]: 0,
          });
        }),
      ];

  const state = createEmptyState('mv');

  const result = getStateAfterActions(
    state,
    ...addPlayerActions,
    ...createZoneActions,
    ...createCardActions,
    ...setVarActions
  );
  return result;
}
