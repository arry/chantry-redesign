import { ZoneDescriptors } from '../creation';

export const mvZoneDescriptors: ZoneDescriptors = {
  library: {
    zoneKind: 'bunch',
    initAccess: 'none',
  },
  hand: {
    zoneKind: 'bunch',
    initAccess: 'owner',
  },
  crypt: {
    zoneKind: 'bunch',
    initAccess: 'none',
  },
  uncontrolled: {
    zoneKind: 'region',
    initAccess: 'owner',
  },
  ready: {
    zoneKind: 'region',
    initAccess: 'all',
  },
  support: {
    zoneKind: 'region',
    initAccess: 'all',
  },
  ashHeap: {
    zoneKind: 'bunch',
    initAccess: 'all',
  },
  current: {
    zoneKind: 'region',
    initAccess: 'all',
  },
  torpor: {
    zoneKind: 'region',
    initAccess: 'all',
  },
};
