import { getAllFinishedGamesForUser, getAllFinishedGames } from './db';

// return { wins, ties, losses }
function calculateUserWtl(username, games) {
  let wins = 0,
    ties = 0,
    losses = 0;

  for (const game of games) {
    const { outcome } = game;
    const [mode, _submode, winner] = outcome;

    if (mode === 'tie') {
      ties++;
    } else if (winner === username) {
      wins++;
    } else {
      losses++;
    }
  }
  return { total: games.length, wins, ties, losses };
}

function filterByRole(username, games, role) {
  return games.filter((game) => {
    const playerNameToRole = game.runtimeInfo['player-name->role'];
    const yourRole = playerNameToRole[username];
    return yourRole === role;
  });
}

async function calculateUserStats(username) {
  const games = await getAllFinishedGamesForUser(username);

  return {
    wtl: calculateUserWtl(username, games),
    wtlByRole: {
      s: calculateUserWtl(username, filterByRole(username, games, 's')),
      u: calculateUserWtl(username, filterByRole(username, games, 'u')),
    },
  };
}

function calculateHighLevel(games) {
  let sWins = 0,
    uWins = 0,
    ties = 0;

  for (const game of games) {
    const { outcome } = game;
    const [mode, _submode, winner] = outcome;

    const playerNameToRole = game.runtimeInfo['player-name->role'];

    if (mode === 'tie') {
      ties++;
    } else {
      if (playerNameToRole[winner] === 's') {
        sWins++;
      } else {
        uWins++;
      }
    }
  }
  return { total: games.length, ties, sWins, uWins };
}

async function calculateSiteStats() {
  const games = await getAllFinishedGames();
  return {
    highLevel: calculateHighLevel(games),
  };
}

export async function calculateStatsForOptionalUser(username) {
  const result = {
    siteStats: await calculateSiteStats(),
  };
  if (username) {
    result.userStats = await calculateUserStats(username);
  }

  return result;
}
