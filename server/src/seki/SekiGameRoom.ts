import logger from '../logger';
import { GameRoom } from '../GameRoom';
import {
  createSekiState,
  State,
  stateToVState,
  getStateVar,
  collectOrders,
  ordersToVOrders,
  validateFilledOrder,
  executeFilledOrder,
  deltaToVDelta,
  Role,
} from '@cr/engine';
import {
  saveEngineCheckpoint,
  getEngineCheckpoint,
  setGameRuntimeInfo,
} from '../db';

function getPovsOfUsername(game, state, username): Role[] {
  const { kind } = game;
  if (kind === 'sandbox') {
    if (game.ownerUsername === username) {
      return state.roles;
    } else {
      return ['spec'];
    }
  } else {
    const role = state.roles.find(
      (thatRole) => state.roleToPlayerName[thatRole] === username
    );
    if (!role) {
      return ['spec'];
    }

    return [role];
  }
}

function getRuntimeInfoFromState(state) {
  const orders = collectOrders(state);
  const { roleToPlayerName } = state;
  const currentPlayers = [
    ...new Set(orders.map((order) => order.role)).values(),
  ].map((role) => {
    return roleToPlayerName[role];
  });

  return {
    ['current-players']: currentPlayers,
    timing: {
      week: getStateVar(state, 'week'),
      step: getStateVar(state, 'step'),
      letter: getStateVar(state, 'letter', null),
      firstPlayer: getStateVar(state, 'firstPlayer', null),
      phasingPlayerLabel: getStateVar(state, 'phasingPlayerLabel', null),
      phase: getStateVar(state, 'phase', null),
    },
  };
}

const TESTING_DISABLE_PERSIST = false;

type PersistedBox = {
  state: State;
  checkpoints: State[];
};

export class SekiGameRoom extends GameRoom {
  state: State | null;
  checkpoints: State[];
  roomManager: any;
  orders: any | null;

  constructor(roomId, publish, io, shortName, roomManager) {
    super(roomId, publish, io, shortName);

    this.roomManager = roomManager;
    this.orders = null;
    this.checkpoints = [];
  }

  async load() {
    await super.load();

    if (!this.game) {
      return;
    }

    const { gid } = this.game;
    const persistedBox: PersistedBox = await getEngineCheckpoint(gid);

    if (persistedBox) {
      const { state, checkpoints } = persistedBox;

      this.state = state;
      this.orders = collectOrders(state);
      console.log('collected orders', this.orders);

      this.checkpoints = checkpoints;
    } else {
      this.state = null;
    }
  }

  getInitialClientStatesMessageForUser(username) {
    const state = this.state;
    const { shortName } = this.game;

    const povs = getPovsOfUsername(this.game, state, username);

    let firstActivePov = null;
    let clientStates = Object.fromEntries(
      povs.map((pov) => {
        const vState = stateToVState(state, pov);

        const vOrders = ordersToVOrders(this.orders, pov);

        if (vOrders.length > 0) {
          firstActivePov = pov;
        }
        return [
          pov,
          {
            ...vState,
            // It feels unclean to extend vState with orders, but on the other
            // hand maybe having orders out-of-band with state is not the best
            // design, and they should be stored on state in the first place.
            vOrders,
          },
        ];
      })
    );

    const message = {
      tag: 'initialClientStates',
      shortName,
      data: {
        yourRole: firstActivePov || povs[0],
        allowedPovs: povs,
        clientStates,
        nCheckpoints: this.checkpoints.length,
      },
    };

    return message;
  }

  async onGameByShortNameStarted(shortName) {
    await super.onGameByShortNameStarted(shortName);

    logger.debug('onGameByShortNameStarted', shortName);
    const { players, gameSettings, gid } = this.game;
    const descriptor = {
      players,
      gameSettings,
    };
    const state = createSekiState(descriptor);
    logger.debug('created seki state', state);

    this.replaceState(state);

    await this.persistState();
  }

  async replaceState(state) {
    this.state = state;
    this.orders = collectOrders(this.state);

    this.notifyAllDynamic((username) => {
      const message = this.getInitialClientStatesMessageForUser(username);
      return message;
    });
  }

  async userEnters(userId, username) {
    await super.userEnters(userId, username);

    if (!this.state) {
      const { shortName, status } = this.game;
      if (status === 'running') {
        await this.onGameByShortNameStarted(shortName);
      }
      return;
    }

    const message = this.getInitialClientStatesMessageForUser(username);
    this.sendToUser(userId, message);
  }

  async userSendsAnswer(userId, username, role, answer) {
    const filledOrder = answer;
    const order = this.orders.find(
      (order) =>
        order.role === role &&
        order.disambiguationNumber === filledOrder.disambiguationNumber
    );

    if (!order) {
      console.warn(
        'User sent a filledOrder that does not point to any order',
        role,
        filledOrder,
        this.orders
      );
      return;
    }

    const validationResult = validateFilledOrder(filledOrder, order, role);
    if (!validationResult.isValid) {
      console.warn(
        'User send an invalid filledOrder',
        role,
        filledOrder,
        order,
        validationResult
      );
      // TODO might send validationResult.errors to the user
      return;
    }

    let next, delta;
    try {
      const res = executeFilledOrder(this.state, order, filledOrder);
      next = res.next;
      delta = res.delta;
    } catch (e) {
      console.warn('Error while executing filledOrder', role, filledOrder, e);
      return;
    }

    this.state = next;
    this.orders = collectOrders(this.state);
    console.log('collected orders', this.orders);

    if (TESTING_DISABLE_PERSIST) {
      // do nothing
    } else {
      await this.persistState();

      if (this.state.outcome) {
        const { gid } = this.game;
        const outcome = this.state.outcome;
        await this.roomManager.finishGameByGid(gid, outcome);
      }
    }

    await this.broadcastDelta(delta);
  }

  async broadcastDelta(delta) {
    const state = this.state;

    this.notifyAllDynamic((username) => {
      const povs = getPovsOfUsername(this.game, state, username);

      const povToVDeltaUpdate = Object.fromEntries(
        povs.map((pov) => {
          return [
            pov,
            {
              vDelta: deltaToVDelta(delta, pov),
              vOrders: ordersToVOrders(this.orders, pov),
            },
          ];
        })
      );

      const message = {
        tag: 'deltas',
        shortName: this.shortName,
        data: povToVDeltaUpdate,
      };
      return message;
    });
  }

  async persistState() {
    const state = this.state;

    const { gid } = this.game;

    const persistedBox = {
      state,
      checkpoints: this.checkpoints,
    };

    const saveResult = await saveEngineCheckpoint(gid, persistedBox);
    if (!saveResult) {
      logger.warn('Could not save engine checkpoint on answer', saveResult);
    }
    const runtimeInfo = getRuntimeInfoFromState(state);
    const riSaveResult = await setGameRuntimeInfo(gid, runtimeInfo);
    if (!riSaveResult) {
      logger.warn('Could not save runtime info on answer', riSaveResult);
    }
    this.roomManager.reloadGameByGid(gid);
  }

  async userSavesCheckpoint(userId, username) {
    const { kind, shortName } = this.game;

    if (!(kind === 'sandbox')) {
      // Sending an error would've been good here.
      return;
    }

    this.checkpoints.push(this.state);
    this.persistState();

    this.sendToUser(userId, {
      tag: 'ackSaveCheckpoint',
      shortName,
      data: {
        nCheckpoints: this.checkpoints.length,
      },
    });
  }

  async userRestoresCheckpoint(userId, username, checkpointIndex) {
    const { kind } = this.game;

    if (!(kind === 'sandbox')) {
      console.warn(
        'userSavesCheckpoint: not in sandbox',
        this.game.shortName,
        username,
        checkpointIndex
      );
      // Sending an error would've been good here.
      return;
    }

    const checkpoints = this.checkpoints;

    if (!(0 <= checkpointIndex && checkpointIndex < checkpoints.length)) {
      console.warn(
        'userSavesCheckpoint: checkpointIndex out of range',
        this.game.shortName,
        username,
        checkpointIndex,
        checkpoints.length
      );
      return;
    }

    const stateFromCheckpoint = checkpoints[checkpointIndex];

    this.replaceState(stateFromCheckpoint);

    // Note: intentionally not persisting here.  Restoring might've been a
    // misclick, so a refresh fixes it; but if it's intentional, then the next
    // action submitted would persist anyway.
  }
}
