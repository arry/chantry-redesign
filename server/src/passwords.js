import bcrypt from 'bcryptjs';
import opst from 'owasp-password-strength-test';

export function generatePasswordHash(password) {
  return bcrypt.hashSync(password);
}

export function checkPasswordAgainstHash(password, passwordHash) {
  return bcrypt.compareSync(password, passwordHash);
}

export function validatePasswordStrength(password, validationErrors) {
  const tested = opst.test(password);
  const { errors } = tested;
  for (const error of errors) {
    validationErrors.push({
      tag: `opst:${error}`,
      field: 'password',
      message: error,
    });
  }
  return errors.length === 0;
}
