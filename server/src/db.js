import { Pool } from 'pg';
import logger from './logger';

export const pool = new Pool();

async function retrieve(name, text, values, resultConverter, rowConverter) {
  try {
    const res = await pool.query({
      name,
      text,
      values,
      rowMode: 'array',
    });
    return resultConverter(res, rowConverter);
  } catch (e) {
    logger.warn('db error', name, values, e);
    return null;
  }
}

async function doUpdate(name, text, values) {
  try {
    const res = await pool.query({
      name,
      text,
      values,
      rowMode: 'array',
    });
    return res.rowCount;
  } catch (e) {
    logger.warn('db error', name, values, e);
    return null;
  }
}

async function doInsert(name, text, values) {
  try {
    const res = await pool.query({
      name,
      text,
      values,
    });
    return res.rowCount;
  } catch (e) {
    logger.warn('db error', name, values, e);
    return null;
  }
}

function convertRowOfSingleValue(row) {
  const [value] = row;
  return value;
}

export async function getCountOfGames() {
  const res = await pool.query({
    text: 'SELECT count(*) from games',
    rowMode: 'array',
  });
  const result = Number(res.rows[0][0]);
  return result;
}

function convertUserData(row) {
  const [
    username,
    passwordHash,
    email,
    createdAt,
    hasAvatar,
    lastSeenAt,
    accountSettings,
    gameSettingsOnLastCreation,
  ] = row;
  const user = {
    username,
    email,
    createdAt,
    hasAvatar,
    lastSeenAt,
    accountSettings,
    gameSettingsOnLastCreation,
  };
  return { user, passwordHash };
}

function convertPendingRegistration(row) {
  const [email, code, isConfirmed] = row;
  const pendingRegistration = {
    email,
    code,
    isConfirmed,
  };
  return pendingRegistration;
}

const PASSWORD_RECOVERY_COLS = ['email', 'recovery_code', 'created_at'];

function convertPasswordRecovery(row) {
  const [email, code, createdAt] = row;
  const passwordRecovery = {
    email,
    code,
    createdAt,
  };
  return passwordRecovery;
}

function convertSingleResult(res, rowConverter) {
  if (res.rowCount === 0) {
    return null;
  }
  if (res.rowCount > 1) {
    logger.warn(
      'expected single result but got more:',
      rowConverter,
      res.rowCount
    );
  }
  const row = res.rows[0];
  return rowConverter(row);
}

function convertAllResults(res, rowConverter) {
  return res.rows.map((row) => rowConverter(row));
}

function convertOneUserData(res) {
  return convertSingleResult(res, convertUserData);
}

export async function findUserDataByUsernameOrEmail(usernameOrEmail) {
  try {
    const res = await pool.query({
      name: 'findUserDataByUsernameOrEmail',
      text: 'SELECT * from users where lower(username)=lower($1) or lower(email)=lower($1)',
      values: [usernameOrEmail],
      rowMode: 'array',
    });
    return convertOneUserData(res);
  } catch (e) {
    logger.warn(
      'db error',
      'findUserDataByUsernameOrEmail',
      usernameOrEmail,
      e
    );
    return null;
  }
}

export async function findUserDataByEmail(email) {
  try {
    const res = await pool.query({
      name: 'findUserDataByEmail',
      text: 'SELECT * from users where lower(email)=lower($1)',
      values: [email],
      rowMode: 'array',
    });
    return convertOneUserData(res);
  } catch (e) {
    logger.warn('db error', 'findUserDataByEmail', email, e);
    return null;
  }
}

export async function findUserDataByExactUsername(username) {
  try {
    const res = await pool.query({
      name: 'findUserDataByExactUsername',
      text: 'SELECT * from users where username=$1',
      values: [username],
      rowMode: 'array',
    });
    return convertOneUserData(res);
  } catch (e) {
    logger.warn('db error', 'findUserDataByExactUsername', username, e);
    return null;
  }
}

export async function findUserDataByCaseInsensitiveUsername(username) {
  try {
    const res = await pool.query({
      name: 'findUserDataByCaseInsensitiveUsername',
      text: 'SELECT * from users where lower(username)=lower($1)',
      values: [username],
      rowMode: 'array',
    });
    return convertOneUserData(res);
  } catch (e) {
    logger.warn('db error', 'findUserDataByExactUsername', username, e);
    return null;
  }
}

export async function findUsernamesByPrefix(usernamePrefix) {
  try {
    const res = await pool.query({
      name: 'findUsernamesByPrefix',
      text: `SELECT username from users
where lower(username) like concat(lower($1), '%')
order by username`,
      values: [usernamePrefix],
      rowMode: 'array',
    });
    return convertAllResults(res, convertRowOfSingleValue);
  } catch (e) {
    logger.warn('db error', 'findUsernamesByPrefix', usernamePrefix, e);
    return null;
  }
}

export async function setUserLastSeenNow(username) {
  try {
    await pool.query({
      name: 'setUserLastSeenNow',
      text: 'update users set last_seen_at=now() where username=$1',
      values: [username],
    });
  } catch (e) {
    logger.warn('db error', 'setUserLastSeenNow', username, e);
    return;
  }
}

export async function closeDb() {
  await pool.end();
}

export async function createUser(data) {
  const { username, passwordHash, email, allowGameEmails } = data;
  const accountSettings = {
    'allow-game-emails': allowGameEmails,
  };
  try {
    const res = await pool.query({
      name: 'createUser',
      text: `
insert into users (username, password_hash, email, account_settings)
values ($1, $2, $3, $4)
            `,
      values: [username, passwordHash, email, accountSettings],
    });
    return res.rowCount;
  } catch (e) {
    logger.warn('db error', 'createUser', data, e);
    return null;
  }
}

export async function updateUserAccountSettings(username, allowGameEmails) {
  const accountSettings = {
    'allow-game-emails': allowGameEmails,
  };

  return await doUpdate(
    'updateUserAccountSettings',
    `update users
set account_settings=$2
where username=$1
        `,
    [username, accountSettings]
  );
}

function convertOnePendingRegistration(res) {
  return convertSingleResult(res, convertPendingRegistration);
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function convertOnePasswordRecovery(res) {
  return convertSingleResult(res, convertPasswordRecovery);
}

export async function createPendingRegistration(data) {
  const { email, code } = data;
  try {
    await pool.query({
      name: 'createPendingRegistration',
      text: `insert into pending_registrations (email, confirmation_code)
values ($1, $2)`,
      values: [email, code],
    });
    return true;
  } catch (e) {
    logger.warn('db error', 'createPendingRegistration', email, code, e);
    return false;
  }
}

export async function findPendingRegistrationByEmail(email) {
  try {
    const res = await pool.query({
      name: 'findPendingRegistrationByEmail',
      text: `select * from pending_registrations
where lower(email) = lower($1)`,
      values: [email],
      rowMode: 'array',
    });
    return convertOnePendingRegistration(res);
  } catch (e) {
    logger.warn('db error', 'findPendingRegistrationByEmail', email, e);
    return null;
  }
}

export async function findPendingRegistrationByCode(code) {
  try {
    const res = await pool.query({
      name: 'findPendingRegistrationByCode',
      text: `select * from pending_registrations
where confirmation_code = $1`,
      values: [code],
      rowMode: 'array',
    });
    return convertOnePendingRegistration(res);
  } catch (e) {
    logger.warn('db error', 'findPendingRegistrationByCode', code, e);
    return null;
  }
}

export async function confirmPendingRegistrationByCode(code) {
  return await doUpdate(
    'confirmPendingRegistrationByCode',
    `update pending_registrations set is_confirmed=true
where confirmation_code = $1`,
    [code]
  );
}

export async function createPasswordRecovery(data) {
  const { email, code } = data;
  try {
    await pool.query({
      name: 'createPasswordRecovery',
      text: `insert into password_recoveries (email, recovery_code)
values ($1, $2)`,
      values: [email, code],
    });
    return true;
  } catch (e) {
    logger.warn('db error', 'createPasswordRecovery', email, code, e);
    return false;
  }
}

export async function findPasswordRecoveryByEmail(email) {
  return await retrieve(
    'findPasswordRecoveryByEmail',
    `select ${PASSWORD_RECOVERY_COLS} from password_recoveries
        where lower(email) = lower($1)`,
    [email],
    convertSingleResult,
    convertPasswordRecovery
  );
}

export async function findPasswordRecoveryByCode(code) {
  return await retrieve(
    'findPasswordRecoveryByCode',
    `select ${PASSWORD_RECOVERY_COLS} from password_recoveries
        where code = $1`,
    [code],
    convertSingleResult,
    convertPasswordRecovery
  );
}

export async function deletePasswordRecoveryByEmail(email) {
  try {
    const res = await pool.query({
      name: 'deletePasswordRecoveryByEmail',
      text: `delete from password_recoveries
where lower(email) = lower($1)`,
      values: [email],
    });
    return res.rowCount;
  } catch (e) {
    logger.warn('db error', 'deletePasswordRecoveryByEmail', email, e);
    return false;
  }
}

export async function setPasswordHashOfUserByEmail(passwordHash, email) {
  return await doUpdate(
    'setPasswordHashOfUserByEmail',
    `update users set password_hash= $1
        where email = $2`,
    [passwordHash, email]
  );
}

const ROOM_MESSAGE_COLS = 'created_at, message_code, args';

function convertRoomMessage(row) {
  const [createdAt, messageCode, args] = row;
  return { createdAt, messageCode, args };
}

export async function addRoomMessage(roomId, code, args) {
  try {
    const res = await pool.query({
      name: 'addRoomMessage',
      text: `insert into room_messages (room_id, message_code, args)
values ($1, $2, $3)
returning ${ROOM_MESSAGE_COLS}
            `,
      values: [roomId, code, args],
      rowMode: 'array',
    });
    return convertSingleResult(res, convertRoomMessage);
  } catch (e) {
    logger.warn('db error', 'addRoomMessage', roomId, code, args, e);
    return null;
  }
}

export async function getRoomMessages(roomId) {
  try {
    const res = await pool.query({
      name: 'getRoomMessages',
      text: `
select ${ROOM_MESSAGE_COLS}
from room_messages
where room_id = $1
order by created_at asc
            `,
      values: [roomId],
      rowMode: 'array',
    });
    return convertAllResults(res, convertRoomMessage);
  } catch (e) {
    logger.warn('db error', 'getRoomMessages', roomId, e);
    return null;
  }
}

export async function getRoomMessagesLimited(roomId, limit) {
  try {
    const res = await pool.query({
      name: 'getRoomMessagesLimited',
      text: `
select ${ROOM_MESSAGE_COLS} from (
  select ${ROOM_MESSAGE_COLS}
  from room_messages
  where room_id = $1
  order by created_at desc
  fetch first $2 rows only
) as limited
order by created_at asc
            `,
      values: [roomId, limit],
      rowMode: 'array',
    });
    return convertAllResults(res, convertRoomMessage);
  } catch (e) {
    logger.warn('db error', 'getRoomMessagesLimited', roomId, e);
    return null;
  }
}

const GAME_COLS =
  'gid, code, kind, status, created_at, owner_username, comment, game_settings, players, last_modified_at, runtime_info, outcome, short_name';

function convertGame(row) {
  const [
    gid,
    code,
    kind,
    status,
    createdAt,
    ownerUsername,
    comment,
    gameSettings,
    players,
    lastModifiedAt,
    runtimeInfo,
    outcome,
    shortName,
  ] = row;

  return {
    gid,
    code,
    kind,
    status,
    createdAt,
    ownerUsername,
    comment,
    gameSettings,
    players,
    lastModifiedAt,
    runtimeInfo,
    outcome,
    shortName,
  };
}

function convertPersonalNotesContent(row) {
  const [content] = row;
  return content || '';
}

export async function createGame(
  username,
  code,
  kind,
  comment,
  gameSettings,
  options = {}
) {
  let initialPlayerNames;

  if (options.playerNames) {
    initialPlayerNames = options.playerNames;
  } else if (options.inviteeUsername) {
    initialPlayerNames = [username, options.inviteeUsername];
  } else {
    initialPlayerNames = [username];
  }

  const status = kind === 'sandbox' ? 'running' : 'open';

  try {
    const res = await pool.query({
      name: 'createGame',
      text: `insert into games
(code, kind, status, owner_username, comment, game_settings, players, short_name)
values (
  $1, $2, $3, $4, $5, $6, $7,
  concat(
    cast($1 as varchar(10)), '-',
    nextval(concat('games_short_name_', $1, '_seq'))
  )
)
returning ${GAME_COLS}
            `,
      values: [
        code,
        kind,
        status,
        username,
        comment,
        gameSettings,
        initialPlayerNames,
      ],
      rowMode: 'array',
    });
    return convertSingleResult(res, convertGame);
  } catch (e) {
    logger.warn(
      'db error',
      'createGame',
      username,
      kind,
      comment,
      gameSettings,
      e
    );
    return null;
  }
}

export async function deleteGame(gid) {
  try {
    const res = await pool.query({
      name: 'deleteGame',
      text: `delete from games where gid=$1`,
      values: [gid],
    });
    return true;
  } catch (e) {
    logger.warn('db error', 'deleteGame', gid, e);
    return null;
  }
}

export async function findGameByGid(gid) {
  return await retrieve(
    'findGameByGid',
    `select ${GAME_COLS} from games where gid=$1`,
    [gid],
    convertSingleResult,
    convertGame
  );
}

export async function findGameByShortName(shortName) {
  return await retrieve(
    'findGameByShortName',
    `select ${GAME_COLS} from games where short_name=$1`,
    [shortName],
    convertSingleResult,
    convertGame
  );
}

// hgames = not sandbox; h for human
const WITH_HGAMES = `with hgames as
(
  select ${GAME_COLS} from games
  where kind in ('async', 'live')
)`;

export async function getLobbyGames() {
  return await retrieve(
    'getLobbyGames',
    `${WITH_HGAMES}
select ${GAME_COLS} from
(
  (select * from hgames as t1
  where (status in ('open', 'running')))
union
  (select * from hgames as t2
  where (status = 'finished')
  order by gid desc
  limit 10)
union
  (select * from hgames as t3
  where (status not in ('canceled', 'expired', 'open', 'running', 'finished'))
  order by gid desc
  limit 10)
) as derived
order by gid desc
        `,
    [],
    convertAllResults,
    convertGame
  );
}

export async function getRecentFinishedGamesForUser(username) {
  return await retrieve(
    'getRecentFinishedGamesForUser',
    `${WITH_HGAMES}
select ${GAME_COLS} from hgames
where status = 'finished'
and $1 = any(players)
order by created_at desc
limit 3
        `,
    [username],
    convertAllResults,
    convertGame
  );
}

export async function getAllFinishedGamesForUser(username) {
  return await retrieve(
    'getAllFinishedGamesForUser',
    `${WITH_HGAMES} select ${GAME_COLS} from hgames
where status = 'finished'
and $1 = any(players)
order by created_at desc
        `,
    [username],
    convertAllResults,
    convertGame
  );
}

export async function getAllFinishedGames() {
  return await retrieve(
    'getAllFinishedGames',
    `${WITH_HGAMES} select ${GAME_COLS} from hgames
where status = 'finished'
order by created_at desc
        `,
    [],
    convertAllResults,
    convertGame
  );
}

export async function getPastGamesForUser(username) {
  return await retrieve(
    'getPastGamesForUser',
    `${WITH_HGAMES} select ${GAME_COLS} from hgames
where status in ('finished', 'voided', 'abandoned')
and $1 = any(players)
order by created_at desc
        `,
    [username],
    convertAllResults,
    convertGame
  );
}

export async function getSandboxGamesForUser(username) {
  return await retrieve(
    'getSandboxGamesForUser',
    `select ${GAME_COLS} from games
where kind = 'sandbox'
and owner_username = $1
and status = 'running'
order by created_at desc
        `,
    [username],
    convertAllResults,
    convertGame
  );
}

export async function setGameStatus(gid, status) {
  return await doUpdate(
    'setGameStatus',
    `update games set status=$2
where gid = $1`,
    [gid, status]
  );
}

export async function setGamePlayers(gid, players) {
  return await doUpdate(
    'setGamePlayers',
    `update games set players=$2
where gid=$1`,
    [gid, players]
  );
}

export async function setGameRuntimeInfo(gid, runtimeInfo) {
  return await doUpdate(
    'setGameRuntimeInfo',
    `update games set runtime_info=$2
where gid=$1`,
    [gid, runtimeInfo]
  );
}

export async function finishGameAndSetOutcome(gid, outcome) {
  return await doUpdate(
    'setGameRuntimeInfo',
    `update games set outcome=$2, status='finished'
where gid=$1`,
    [gid, outcome]
  );
}

export async function saveEngineCheckpoint(gid, engineCheckpoint) {
  return await doInsert(
    'saveEngineCheckpoint',
    `insert into game_engine_checkpoints (gid, engine_checkpoint)
values ($1, $2)
on conflict (gid) do update set engine_checkpoint = $2
`,
    [gid, Buffer.from(JSON.stringify(engineCheckpoint))]
  );
}

export function convertEngineCheckpoint(row) {
  const [engineCheckpoint] = row;
  if (!engineCheckpoint) {
    return null;
  }
  return JSON.parse(engineCheckpoint.toString());
}

export async function getEngineCheckpoint(gid) {
  return await retrieve(
    'findEngineCheckpoint',
    `select engine_checkpoint from game_engine_checkpoints
where gid=$1`,
    [gid],
    convertSingleResult,
    convertEngineCheckpoint
  );
}

export async function getPersonalNotesContent(username, gid) {
  return await retrieve(
    'getPersonalNotesContent',
    `select content from personal_notes
where username=$1 and gid=$2`,
    [username, gid],
    convertSingleResult,
    convertPersonalNotesContent
  );
}

export async function setPersonalNotesContent(username, gid, content) {
  try {
    await pool.query({
      name: 'setPersonalNotesContent',
      text: `insert into personal_notes (username, gid, content)
values ($1, $2, $3)
on conflict (username, gid) do update set content = $3
            `,
      values: [username, gid, content],
    });
    return true;
  } catch (e) {
    logger.warn(
      'db error',
      'setPersonalNotesContent',
      username,
      gid,
      content,
      e
    );
    return false;
  }
}
