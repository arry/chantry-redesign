import shortid from 'shortid';

export function generatePendingRegistrationCode() {
  return shortid.generate();
}

export function generatePasswordResetCode() {
  return shortid.generate();
}
