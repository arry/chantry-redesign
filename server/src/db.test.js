/* eslint-disable no-undef */
import {
  getCountOfGames,
  closeDb,
  createPendingRegistration,
  findPendingRegistrationByEmail,
  findPendingRegistrationByCode,
  confirmPendingRegistrationByCode,
  createUser,
  findUserDataByUsernameOrEmail,
  pool,
  addRoomMessage,
  getRoomMessages,
  getRoomMessagesLimited,
  createGame,
  setGamePlayers,
  findGameByGid,
} from './db';

import fs from 'fs';

async function recreateSchema() {
  const drop = fs.readFileSync('resources/drop-schema.sql', 'utf-8');
  try {
    await pool.query(drop);
  } catch (e) {
    // Ignore the error.
  }

  const apply = fs.readFileSync('resources/apply-schema.sql', 'utf-8');
  await pool.query(apply);
}

beforeAll(() => {
  return recreateSchema();
});

test('count of games is zero initially', async () => {
  const count = await getCountOfGames();
  expect(count).toBe(0);
});

afterAll(() => {
  return closeDb();
});

test('pending registrations', async () => {
  const nonExistent = await findPendingRegistrationByCode('foo');
  expect(nonExistent).toBeNull();

  const wasCreated = await createPendingRegistration({
    email: 'foo@bar.com',
    code: 'foo',
  });
  expect(wasCreated).toBeTruthy();

  const expected = {
    email: 'foo@bar.com',
    code: 'foo',
    isConfirmed: false,
  };

  const thatOne = await findPendingRegistrationByCode('foo');
  expect(thatOne).toEqual(expected);

  const again = await findPendingRegistrationByEmail('foo@bar.com');
  expect(again).toEqual(expected);

  const wasConfirmed = await confirmPendingRegistrationByCode('foo');
  expect(wasConfirmed).toBeTruthy();

  const confirmed = await findPendingRegistrationByEmail('foo@bar.com');
  expect(confirmed).toEqual({ ...expected, isConfirmed: true });
});

test('create user', async () => {
  const wasCreated = await createUser({
    username: 'mary',
    passwordHash: '123',
    email: 'foo@bar.com',
  });
  expect(wasCreated).toBeTruthy();

  const userData = await findUserDataByUsernameOrEmail('FOO@BAR.COM');
  expect(userData).toMatchObject({
    passwordHash: '123',
    user: {
      username: 'mary',
      email: 'foo@bar.com',
      accountSettings: null,
      gameSettingsOnLastCreation: {},
      createdAt: expect.any(Date),
      lastSeenAt: expect.any(Date),
      hasAvatar: false,
    },
  });
});

test('room messages', async () => {
  const wasAdded = await addRoomMessage('lobbyRoom', 'chat', {
    author: 'mary',
    text: 'hi everybody',
  });
  expect(wasAdded).toMatchObject({
    messageCode: 'chat',
    args: {
      author: 'mary',
      text: 'hi everybody',
    },
  });

  const lobbyMessages = await getRoomMessages('lobbyRoom');

  expect(lobbyMessages).toMatchObject([
    {
      messageCode: 'chat',
      args: { author: 'mary', text: 'hi everybody' },
      createdAt: expect.any(Date),
    },
  ]);

  await addRoomMessage('gameRoom:1', 'userEnters', { username: 'mary' });
  await addRoomMessage('gameRoom:1', 'chat', {
    author: 'mary',
    text: 'and here',
  });

  const gameMessages = await getRoomMessages('gameRoom:1');
  expect(gameMessages).toMatchObject([
    {
      messageCode: 'userEnters',
      args: { username: 'mary' },
      createdAt: expect.any(Date),
    },
    {
      messageCode: 'chat',
      args: { author: 'mary', text: 'and here' },
      createdAt: expect.any(Date),
    },
  ]);

  await addRoomMessage('lobbyRoom', 'chat', {
    author: 'mary',
    text: 'foo',
  });
  await addRoomMessage('lobbyRoom', 'chat', {
    author: 'mary',
    text: 'bar',
  });
  await addRoomMessage('lobbyRoom', 'chat', {
    author: 'mary',
    text: 'baz',
  });

  const limitedLobbyMessages = await getRoomMessagesLimited('lobbyRoom', 2);
  expect(limitedLobbyMessages).toMatchObject([
    {
      messageCode: 'chat',
      args: { author: 'mary', text: 'bar' },
      createdAt: expect.any(Date),
    },
    {
      messageCode: 'chat',
      args: { author: 'mary', text: 'baz' },
      createdAt: expect.any(Date),
    },
  ]);
});

test('setGamePlayers', async () => {
  const username = 'mary',
    kind = 'async',
    comment = 'Anyone welcome',
    gameSettings = {};

  const game = await createGame(username, kind, comment, gameSettings);

  const { gid } = game;

  const res = await setGamePlayers(gid, ['mary', 'john']);
  expect(res).toBeTruthy();

  const newGame = await findGameByGid(gid);
  expect(newGame.players).toEqual(['mary', 'john']);
});
