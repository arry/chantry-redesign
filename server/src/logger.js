function isoTime() {
  return `${new Date().toISOString()}`;
}

const logger = {
  debug(...args) {
    this.output('DEBUG', ...args);
  },

  log(...args) {
    this.output('LOG', ...args);
  },

  info(...args) {
    this.output('INFO', ...args);
  },

  warn(...args) {
    this.output('WARN', ...args);
  },

  error(...args) {
    this.outputError('ERROR', ...args);
  },

  output(level, ...args) {
    console.log(isoTime(), level, ...args);
  },

  outputError(level, ...args) {
    console.error(isoTime(), level, ...args);
  },
};

export default logger;
