import {
  getRoomMessagesLimited,
  getLobbyGames,
  getRecentFinishedGamesForUser,
} from './db';
import logger from './logger';
import { BaseRoom } from './BaseRoom';
import { Game } from './types';

const LOBBY_MESSAGES_LIMIT = 200;

export class LobbyRoom extends BaseRoom {
  games: Game[];

  async load() {
    this.roomMessages = await getRoomMessagesLimited(
      this.roomId,
      LOBBY_MESSAGES_LIMIT
    );

    const games = await getLobbyGames();
    this.setGames(games, 'load');
  }

  setGames(newGames, tag) {
    let hasInvalid = false;
    newGames.forEach((game, index) => {
      if (!(game && game.gid && game.shortName)) {
        logger.error('in setGames', tag, 'a game is not valid:', game, index);
        hasInvalid = true;
      }
    });
    if (hasInvalid) {
      logger.error('setGames', tag, ': aborting');
      return;
    }
    this.games = newGames;
  }

  async loadInitialRoomDataFor(username) {
    const recentFinishedGames = await getRecentFinishedGamesForUser(username);
    return {
      games: this.games,
      recentFinishedGames,
    };
  }

  onGameCreated(game) {
    this.games.push(game);
    this.notifyAll({
      tag: 'lobbyRoom/gameCreated',
      game,
    });

    this.publish({
      tag: 'gameCreated',
      gid: game.gid,
    });
  }

  getGameByShortName(shortName) {
    return this.games.find((game) => game.shortName === shortName);
  }

  onGameByShortNameCanceled(shortName) {
    const newGames = this.games.filter((game) => game.shortName !== shortName);
    const game = this.getGameByShortName(shortName);
    const wasRemoved = newGames.length < this.games.length;
    this.setGames(newGames, 'onGameByShortNameCanceled');
    if (wasRemoved) {
      this.publish({
        tag: 'gameCanceled',
        gid: game.gid,
      });
      this.notifyAll({
        tag: 'lobbyRoom/gameByShortNameCanceled',
        shortName,
      });
    }
  }

  onGameByShortNameDeleted(shortName) {
    this.notifyAll({
      tag: 'lobbyRoom/gameByShortNameDeleted',
      shortName,
    });
  }

  onGameByShortNameChangedPlayers(shortName, newPlayers) {
    const newGames = this.games.map((game) => {
      if (game.shortName !== shortName) {
        return game;
      }
      return { ...game, players: newPlayers };
    });

    const game = this.getGameByShortName(shortName);
    this.setGames(newGames, 'onGameByShortNameChangedPlayers');
    this.notifyAll({
      tag: 'lobbyRoom/gameBydShortNameChangedPlayers',
      shortName,
      newPlayers,
    });

    this.publish({
      tag: 'gameChangedPlayers',
      gid: game.gid,
    });
  }

  onGameByShortNameStarted(shortName) {
    let theGame = null;
    const newGames = this.games.map((game) => {
      if (game.shortName !== shortName) {
        return game;
      }
      theGame = { ...game, status: 'running' };
      return theGame;
    });
    if (!theGame) {
      logger.warn(
        'game is supposed to be started but cannot find it:',
        shortName
      );
      return;
    }

    this.setGames(newGames, 'onGameByShortNameStarted');
    this.notifyAll({
      tag: 'lobbyRoom/gameByShortNameStarted',
      shortName,
    });

    this.publish({
      tag: 'gameStarted',
      gid: theGame.gid,
      players: theGame.players,
      gameSettings: theGame.gameSettings,
    });
  }

  onGameByShortNameFinished(shortName, outcome) {
    let theGame = null;
    const newGames = this.games.map((game) => {
      if (game.shortName !== shortName) {
        return game;
      }
      theGame = { ...game, status: 'finished', outcome };
      return theGame;
    });

    this.setGames(newGames, 'onGameByShortNameFinished');
    this.notifyAll({
      tag: 'lobbyRoom/gameByShortNameFinished',
      shortName,
      outcome,
    });
  }

  onGameByShortNameUpdated(shortName, newGame) {
    let wasRemoved = false;
    const newGames = this.games
      .map((game) => {
        if (game.shortName !== shortName) {
          return game;
        }
        if (!(newGame.status === 'open' || newGame.status === 'running')) {
          wasRemoved = true;
          return null;
        }
        return newGame;
      })
      .filter((game) => !!game);
    this.setGames(newGames, 'onGameByShortNameUpdated');
    if (wasRemoved) {
      // XXX might be something else but canceled - e.g. expired.
      console.log(
        'onGameByShortNameUpdated wasRemoved: sending canceled but might be something else',
        JSON.stringify(newGame)
      );
      this.notifyAll({
        tag: 'lobbyRoom/gameByShortNameCanceled',
        shortName,
      });
    } else {
      this.notifyAll({
        tag: 'lobbyRoom/gameByShortNameUpdated',
        shortName,
        newGame,
      });
    }
  }
}
