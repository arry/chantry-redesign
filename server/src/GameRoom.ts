import {
  getRoomMessages,
  findGameByShortName,
  getPersonalNotesContent,
} from './db';
import logger from './logger';
import { BaseRoom } from './BaseRoom';
import { Game } from './types';

export class GameRoom extends BaseRoom {
  shortName: string;
  game: Game | null;

  constructor(roomId, publish, io, shortName) {
    super(roomId, publish, io);
    this.shortName = shortName;
    this.game = null;
  }

  isGameRoom() {
    return true;
  }

  async load() {
    const { shortName, roomId } = this;

    this.roomMessages = await getRoomMessages(roomId);

    const game = await findGameByShortName(shortName);
    this.game = game;
  }

  getGid() {
    if (!this.game) {
      throw new Error(`game must be loaded to get room's gid`);
    }
    return this.game.gid;
  }

  async loadInitialRoomDataFor(username) {
    if (!this.game) {
      return {
        shortName: this.shortName,
        game: null,
      };
    }

    const personalNotesContent = await getPersonalNotesContent(
      username,
      this.getGid()
    );
    return {
      shortName: this.shortName,
      game: this.game,
      personalNotesContent,
    };
  }

  onGameByShortNameCanceled(shortName) {
    if (shortName !== this.shortName) {
      logger.warn('Not interested in non-my game updates:', shortName);
      return;
    }
    this.notifyAll({
      tag: 'gameRoom/thisGameCanceled',
      shortName,
    });
  }

  onGameByShortNameDeleted(shortName) {
    if (shortName !== this.shortName) {
      logger.warn('Not interested in non-my game updates:', shortName);
      return;
    }
    this.notifyAll({
      tag: 'gameRoom/thisGameDeleted',
      shortName,
    });
  }

  onGameByShortNameChangedPlayers(shortName, newPlayers) {
    if (shortName !== this.shortName) {
      logger.warn('Not interested in non-my game updates:', shortName);
      return;
    }
    this.game.players = newPlayers;
    this.notifyAll({
      tag: 'gameRoom/thisGameChangedPlayers',
      shortName,
      newPlayers,
    });
  }

  onGameByShortNameStarted(shortName) {
    this.game.status = 'running';
    logger.debug('onGameByShortNameStarted', this.game);
    this.notifyAll({
      tag: 'gameRoom/thisGameStarted',
      shortName,
    });
  }

  onGameByShortNameFinished(shortName, outcome) {
    this.game.status = 'finished';
    this.game.outcome = outcome;
    logger.debug('onGameByShortNameFinished', this.game, outcome);
    this.notifyAll({
      tag: 'gameRoom/thisGameFinished',
      shortName,
      outcome,
    });
  }

  onGameByShortNameUpdated(shortName, newGame) {
    this.game = newGame;
    this.notifyAll({
      tag: 'gameRoom/thisGameUpdated',
      shortName,
      newGame,
    });
  }

  handleTalkerMessage(_message) {
    // do nothing
  }

  async userSendsAnswer(_userId, _username, _role, _answer) {
    // do nothing
  }

  async userSavesCheckpoint(_userId, _username) {
    // do nothing
  }

  async userRestoresCheckpoint(_userId, _username, _checkpointIndex) {
    // do nothing
  }
}
