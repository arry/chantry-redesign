import logger from '../logger';
import { GameRoom } from '../GameRoom';
import { gameHasRuntimeState } from '../utils';

export class TdGameRoom extends GameRoom {
  async userEnters(userId, username) {
    const hasRuntimeState = gameHasRuntimeState(this.game);
    await super.userEnters(userId, username);
    this.publish({
      tag: 'userEntersGameRoom',
      gid: this.getGid(),
      userId,
      username,
      hasRuntimeState,
    });
  }

  userLeaves(userId, username) {
    logger.debug('TdGameRoom:userLeaves', userId, username);
    super.userLeaves(userId, username);
    this.publish({
      tag: 'userLeavesGameRoom',
      gid: this.getGid(),
      userId,
      username,
    });
  }

  onGameByShortNameStarted(shortName) {
    super.onGameByShortNameStarted(shortName);

    const userIds = this.getAllUserIds();
    this.publish({
      tag: 'giveRuntimeStatesAfterGameStarted',
      gid: this.getGid(),
      userIds,
    });
  }

  handleTalkerMessage(message) {
    const { userId, tag, data } = message;
    if (tag === 'initial-client-states') {
      const clientStates = data['client-states'];
      const yourRole = data['your-role'];
      this.sendToUser(userId, {
        tag: 'initialClientStates',
        shortName: this.shortName,
        data: {
          clientStates,
          yourRole,
        },
      });
    } else if (tag === 'deltas') {
      this.sendToUser(userId, {
        tag,
        shortName: this.shortName,
        data,
      });
    } else {
      logger.warn('Unrecognized tag of talker message', tag, message);
    }
  }

  async userSendsAnswer(userId, username, role, answer) {
    this.publish({
      tag: 'userSendsAnswer',
      gid: this.getGid(),
      userId,
      username,
      role,
      answer,
    });
  }
}
