import {
  addRoomMessage,
  createGame,
  findGameByGid,
  findGameByShortName,
  setGameStatus,
  deleteGame,
  setGamePlayers,
  findUserDataByExactUsername,
  findUserDataByCaseInsensitiveUsername,
  finishGameAndSetOutcome,
} from './db';
import logger from './logger';
import { mailer } from './mailer';
import { gameHasPlayer } from './utils';
import { LobbyRoom } from './LobbyRoom';
import { TdGameRoom } from './td/TdGameRoom';
import { SekiGameRoom } from './seki/SekiGameRoom';
import { clientServerCommonData } from '@cr/engine';

async function createRoom(roomId, roomManager) {
  const { publish, io } = roomManager;

  let room;
  if (roomId === 'lobbyRoom') {
    room = new LobbyRoom(roomId, publish, io);
  } else {
    const m = /gameRoom:(.+)/.exec(roomId);
    if (!m) {
      logger.error('roomId must be either lobbyRoom or gameRoom:\\d+', roomId);
      throw `Unrecognized roomId: ${roomId}`;
    }
    const shortName = m[1];
    const [code, _rest] = shortName.split('-');
    if (code === 'td') {
      room = new TdGameRoom(roomId, publish, io, shortName);
    } else if (code === 'seki') {
      room = new SekiGameRoom(roomId, publish, io, shortName, roomManager);
    } else {
      logger.warn('Unrecognized code of game room:', code);
      room = new LobbyRoom(roomId, publish, io);
    }
  }
  await room.load();
  return room;
}

function validatePlayerNames(playerNames) {
  const { usernameMinLength } = clientServerCommonData.validationConstants;
  let hasError = false;
  const fieldsValidation = {};

  playerNames.forEach((playerName, index) => {
    if (!(playerName.length >= usernameMinLength)) {
      fieldsValidation[`playerName_${index}`] = [
        {
          tag: 'playerNameTooShort',
          message: `Player name must have at least ${usernameMinLength} characters.`,
        },
      ];
      hasError = true;
    }
  });

  return {
    fieldsValidation,
    hasError,
  };
}

class RoomManager {
  constructor(publish) {
    this.publish = publish;
    this.users = {};
    this.rooms = {};
    this.io = null;
  }

  initIo(io) {
    if (this.io) {
      throw 'Alread has io';
    }
    this.io = io;
    for (const room of Object.values(this.rooms)) {
      room.initIo(io);
    }
  }

  getSocket(userId) {
    return this.users[userId].socket;
  }

  sendToUser(userId, message) {
    const socket = this.getSocket(userId);
    socket.emit('message', message);
  }

  getLobbyRoom() {
    return this.rooms['lobbyRoom'];
  }

  notifyRoomsOnGameChanged(shortName, fn) {
    this.notifyLobbyRoom(fn);
    this.notifyGameRoom(shortName, fn);
  }

  notifyLobbyRoom(fn) {
    const room = this.getLobbyRoom();
    if (!room) {
      return;
    }
    fn(room);
  }

  notifyGameRoom(shortName, fn) {
    const room = this.rooms[`gameRoom:${shortName}`];
    if (!room) {
      return;
    }
    fn(room);
  }

  async changeRoomOfUser(userId, newRoomId) {
    const user = this.users[userId];
    logger.debug(
      'changeRoomOfUser',
      userId,
      user.username,
      user.roomId,
      newRoomId
    );
    const { roomId, username } = user;
    if (roomId === newRoomId) {
      return;
    }

    if (roomId) {
      this.userLeavesRoom(userId, username, roomId);
    }
    if (!newRoomId) {
      throw new Error(`Must have new room id: ${userId}, ${newRoomId}`);
    }
    await this.userEntersRoom(userId, username, newRoomId);
  }

  async userEntersRoom(userId, username, roomId) {
    logger.debug('userEntersRoom', userId, username, roomId);
    if (!this.rooms[roomId]) {
      const room = await createRoom(roomId, this);
      this.rooms[roomId] = room;
    }
    await this.rooms[roomId].userEnters(userId, username);

    const user = this.users[userId];
    const { socket } = user;
    socket.join(roomId);

    user.roomId = roomId;
  }

  userLeavesRoom(userId, username, roomId) {
    logger.debug('userLeavesRoom', userId, username, roomId);
    const room = this.rooms[roomId];
    if (!room) {
      logger.error(
        'userLeavesRoom - room unexistent',
        userId,
        username,
        roomId
      );
      return;
    }
    const user = this.users[userId];
    const { socket } = user;
    socket.leave(roomId);
    room.userLeaves(userId, username);

    if (room.isEmpty() && roomId !== 'lobbyRoom') {
      room.willDestroy();
      this.rooms[roomId] = null;
    }
  }

  checkConditions(userId, tag, conditions) {
    for (const condition of conditions) {
      const { check, text } = condition;
      if (!check()) {
        this.sendToUser(userId, {
          tag,
          text,
        });
        return false;
      }
    }
    return true;
  }

  async sendEmail({ toUser, kind, game, data }) {
    const userData = await findUserDataByExactUsername(toUser);
    if (!userData) {
      logger.warn('sendEmail: cannot find user', toUser);
      return;
    }
    let { accountSettings, email } = userData.user;
    if (accountSettings === null) {
      accountSettings = {};
    }
    const { 'allow-game-emails': allowGameEmails } = accountSettings;
    if (!allowGameEmails) {
      logger.info(
        "sendEmail: not sending because settings don't allow",
        toUser,
        kind,
        data
      );
      return;
    }

    if (game.kind !== 'async') {
      return;
    }

    const { shortName } = game;

    // TODO detect if the user is currently on the site and notify them via
    // another means; then don't mail.

    let result;

    switch (kind) {
      case 'playerJoins': {
        result = await mailer.sendPlayerJoinsEmail(toUser, email, {
          ...data,
          shortName,
        });
        break;
      }
      case 'playerLeaves': {
        result = await mailer.sendPlayerLeavesEmail(toUser, email, {
          ...data,
          shortName,
        });
        break;
      }
      default: {
        logger.warn('Unrecognized sendEmail kind', kind);
        break;
      }
    }

    if (result) {
      logger.info('Sent email', kind, toUser);
    } else {
      logger.warn("Couldn't send email", kind, toUser, data);
    }
  }

  async applyGameAction(userId, actionId, shortName) {
    const user = this.users[userId];
    const { username } = user;

    if (!username) {
      this.sendToUser(userId, {
        tag: 'applyGameActionError',
        text: 'Not allowed to issue game actions',
      });
      return;
    }

    const game = await findGameByShortName(shortName);
    if (!game) {
      this.sendToUser(userId, {
        tag: 'applyGameActionError',
        text: "This game doesn't exist",
      });
      return;
    }
    const { gid } = game;

    if (actionId === 'cancel') {
      const passed = this.checkConditions(userId, 'applyGameActionError', [
        {
          check: () => game.status === 'open',
          text: 'Game must be open to cancel',
        },
        {
          check: () => game.ownerUsername === username,
          text: 'Must be owner to cancel',
        },
      ]);

      if (!passed) {
        return;
      }

      const result = await setGameStatus(gid, 'canceled');
      if (result) {
        this.sendToUser(userId, {
          tag: 'applyGameActionSuccess',
          shortName,
          actionId,
        });
        this.notifyRoomsOnGameChanged(shortName, (room) => {
          room.onGameByShortNameCanceled(shortName);
        });
        this.addGameRoomMessage(shortName, 'gameCanceled', {});
      } else {
        logger.warn(
          'Error while canceling game',
          shortName,
          username,
          JSON.stringify(game)
        );
        this.sendToUser(userId, {
          tag: 'applyGameActionError',
          shortName,
          text: "Couldn't cancel game",
        });
      }
    } else if (actionId === 'delete') {
      const passed = this.checkConditions(userId, 'applyGameActionError', [
        {
          check: () => game.kind === 'sandbox',
          text: 'Game must be sandbox to delete',
        },
        {
          check: () => game.ownerUsername === username,
          text: 'Must be owner to delete',
        },
      ]);

      if (!passed) {
        return;
      }

      const result = await deleteGame(gid);
      if (!result) {
        this.sendToUser(userId, {
          tag: 'applyGameActionError',
          shortName,
          text: "Couldn't delete game",
        });
        return;
      }

      this.sendToUser(userId, {
        tag: 'applyGameActionSuccess',
        shortName,
        actionId,
      });

      this.notifyRoomsOnGameChanged(shortName, (room) => {
        room.onGameByShortNameDeleted(shortName);
      });
    } else if (actionId === 'join') {
      const passed = this.checkConditions(userId, 'applyGameActionError', [
        {
          check: () => game.status === 'open',
          text: 'Game must be open to join',
        },
        {
          check: () => game.players.length < 2,
          text: "Game doesn't have open slots",
        },
        {
          check: () => !gameHasPlayer(game, username),
          text: "Cannot join because you're already in the game",
        },
      ]);
      if (!passed) {
        return;
      }
      const newPlayers = [...game.players, username];
      const result = await setGamePlayers(gid, newPlayers);
      if (result) {
        this.sendToUser(userId, {
          tag: 'applyGameActionSuccess',
          shortName,
          actionId,
        });
        this.notifyRoomsOnGameChanged(shortName, (room) => {
          room.onGameByShortNameChangedPlayers(shortName, newPlayers);
        });
        this.addGameRoomMessage(shortName, 'playerJoins', { username });
        await this.sendEmail({
          toUser: game.ownerUsername,
          kind: 'playerJoins',
          game,
          data: {
            joinerUsername: username,
          },
        });
      } else {
        logger.warn(
          'Error while joining game',
          shortName,
          username,
          JSON.stringify(game)
        );
        this.sendToUser(userId, {
          tag: 'applyGameActionError',
          shortName,
          text: "Couldn't join game",
        });
      }
    } else if (actionId === 'leave') {
      const passed = this.checkConditions(userId, 'applyGameActionError', [
        {
          check: () => game.status === 'open',
          text: 'Game must be open to leave',
        },
        {
          check: () => game.ownerUsername !== username,
          text: 'Cannot leave the game if owner',
        },
        {
          check: () => gameHasPlayer(game, username),
          text: 'Must be in the game to leave',
        },
      ]);
      if (!passed) {
        return;
      }
      const newPlayers = game.players.filter((p) => p !== username);
      const result = await setGamePlayers(gid, newPlayers);
      if (result) {
        this.notifyRoomsOnGameChanged(shortName, (room) => {
          room.onGameByShortNameChangedPlayers(shortName, newPlayers);
        });
        this.addGameRoomMessage(shortName, 'playerLeaves', { username });
        this.sendToUser(userId, {
          tag: 'applyGameActionSuccess',
          shortName,
          actionId,
        });
        await this.sendEmail({
          toUser: game.ownerUsername,
          kind: 'playerLeaves',
          game,
          data: {
            leavingUsername: username,
          },
        });
      } else {
        logger.warn(
          'Error while leaving game',
          shortName,
          username,
          JSON.stringify(game)
        );
        this.sendToUser(userId, {
          tag: 'applyGameActionError',
          shortName,
          text: "Couldn't leave game",
        });
      }
    } else if (actionId === 'start') {
      const passed = this.checkConditions(userId, 'applyGameActionError', [
        {
          check: () => game.status === 'open',
          text: 'Game must be open to start',
        },
        {
          check: () => game.ownerUsername === username,
          text: 'Must be owner to start',
        },
        {
          check: () => game.players.length === 2,
          text: 'Must have two players to start',
        },
      ]);
      if (!passed) {
        return;
      }

      const result = await setGameStatus(gid, 'running');
      if (result) {
        this.sendToUser(userId, {
          tag: 'applyGameActionSuccess',
          shortName,
          actionId,
        });
        await this.addGameRoomMessage(shortName, 'gameStarted', {});
        this.notifyRoomsOnGameChanged(shortName, (room) => {
          room.onGameByShortNameStarted(shortName);
        });
      } else {
        logger.warn(
          'Error while starting game',
          shortName,
          username,
          JSON.stringify(game)
        );
        this.sendToUser(userId, {
          tag: 'applyGameActionError',
          shortName,
          text: "Couldn't start game",
        });
        return;
      }
    } else {
      logger.warn('Unrecognized actionId', username, actionId, shortName);
    }
  }

  // Add the message to database; notify room if it is present.
  async addGameRoomMessage(shortName, code, args) {
    const roomName = `gameRoom:${shortName}`;
    const roomMessage = await addRoomMessage(roomName, code, args);
    const room = this.rooms[roomName];
    if (room) {
      room.roomMessageAdded(roomMessage);
    }
  }

  async userSentMessage(userId, message) {
    const { tag } = message;
    logger.debug('userSentMessage', userId, message);
    switch (tag) {
      case 'enterRoom': {
        const { roomId } = message;
        await this.changeRoomOfUser(userId, roomId);
        break;
      }
      case 'chat': {
        const originalText = message.text;
        const user = this.users[userId];
        const { roomId, username } = user;
        // Note: should correspond to maxlength on client
        const text = originalText.substring(0, 200);
        if (!roomId || !username) {
          this.sendToUser(userId, {
            tag: 'error',
            text: 'Cannot send chat line',
          });
          return;
        }

        await this.rooms[roomId].userSendsChatLine(userId, username, text);
        break;
      }
      case 'createGame': {
        const { code, kind, comment, side, invitee } = message;

        let inviteeUserData;

        const user = this.users[userId];
        const { roomId, username } = user;
        if (!(username && roomId === 'lobbyRoom')) {
          this.sendToUser(userId, {
            tag: 'createGameError',
            text: 'Unauthorized to create game',
          });
          return;
        }

        if (!(code === 'td' || code === 'seki')) {
          this.sendToUser(userId, {
            tag: 'createGameError',
            text: 'Unrecognized game code for creation',
          });
          return;
        }

        if (invitee.length > 0) {
          inviteeUserData = await findUserDataByExactUsername(invitee);
          if (!inviteeUserData) {
            this.sendToUser(userId, {
              tag: 'createGameError',
              text: 'Invitee does not exist',
            });
            return;
          }
        }

        let game;
        // XXX this doesn't validate the side attribute, so a malicious user
        // could create a game with bogus side-selection value.
        try {
          game = await createGame(
            username,
            code,
            kind,
            comment,
            {
              'side-selection': side,
            },
            {
              inviteeUsername: inviteeUserData
                ? inviteeUserData.user.username
                : null,
            }
          );
          if (!game) {
            logger.warn("Couldn't create game", username, message);
            this.sendToUser(userId, {
              tag: 'createGameError',
              text: 'Error creating game',
            });
            return;
          }
        } catch (e) {
          logger.warn("Couldn't create game", username, message, e);
          this.sendToUser(userId, {
            tag: 'createGameError',
            text: 'Error creating game',
          });
          return;
        }
        this.sendToUser(userId, {
          tag: 'createGameSuccess',
          game,
        });

        this.notifyLobbyRoom((lobbyRoom) => {
          lobbyRoom.onGameCreated(game);
        });
        break;
      }
      case 'createSandboxGame': {
        const { code, comment, side, playerNames } = message;

        const user = this.users[userId];
        const { username } = user;
        if (!username) {
          this.sendToUser(userId, {
            tag: 'createSandboxGameError',
            text: 'Unauthorized to create sandbox game',
          });
          return;
        }

        if (!(code === 'seki')) {
          this.sendToUser(userId, {
            tag: 'createSandboxGameError',
            text: 'Unrecognized game code for creation: only seki allowed',
          });
          return;
        }

        const validation = validatePlayerNames(playerNames);
        if (validation.hasError) {
          this.sendToUser(userId, {
            tag: 'createSandboxGameError',
            text: 'Cannot create sandbox game: playerNames is invalid',
          });
          return;
        }

        let game;
        try {
          const kind = 'sandbox';

          game = await createGame(
            username,
            code,
            kind,
            comment,
            {
              'side-selection': side,
            },
            {
              playerNames,
            }
          );
          if (!game) {
            logger.warn("Couldn't create sandbox game", username, message);
            this.sendToUser(userId, {
              tag: 'createSandboxGameError',
              text: 'Error creating game',
            });
            return;
          }
        } catch (e) {
          logger.warn("Couldn't create sandbox game", username, message, e);
          this.sendToUser(userId, {
            tag: 'createSandboxGameError',
            text: 'Error creating game',
          });
          return;
        }
        this.sendToUser(userId, {
          tag: 'createSandboxGameSuccess',
          game,
        });
        break;
      }

      case 'applyGameAction': {
        const { actionId, shortName } = message;
        await this.applyGameAction(userId, actionId, shortName);
        break;
      }
      case 'sendAnswer': {
        const { shortName, role, answer } = message;

        const user = this.users[userId];
        const { roomId, username } = user;

        const room = this.rooms[roomId];

        if (!(username && room.shortName === shortName)) {
          this.sendToUser(userId, {
            tag: 'unathorizedSendAnswer',
            text: 'Must be authenticated and in a game room to answer',
          });
          return;
        }
        await room.userSendsAnswer(userId, username, role, answer);
        break;
      }
      case 'sandboxSaveCheckpoint': {
        const { shortName } = message;

        const user = this.users[userId];
        const { roomId, username } = user;

        const room = this.rooms[roomId];

        if (!(username && room.shortName === shortName)) {
          this.sendToUser(userId, {
            tag: 'unathorizedToSaveCheckpoint',
            text: 'Must be authenticated and in a game room to save checkpoint',
          });
          return;
        }
        await room.userSavesCheckpoint(userId, username);
        break;
      }
      case 'sandboxRestoreCheckpoint': {
        const { shortName, checkpointIndex } = message;

        const user = this.users[userId];
        const { roomId, username } = user;

        const room = this.rooms[roomId];

        if (!(username && room.shortName === shortName)) {
          this.sendToUser(userId, {
            tag: 'unathorizedToRestoreCheckpoint',
            text: 'Must be authenticated and in a game room to restore checkpoint',
          });
          return;
        }
        await room.userRestoresCheckpoint(userId, username, checkpointIndex);
        break;
      }
      default: {
        logger.warn('Unrecognized message from user', userId, message);
        break;
      }
    }
  }

  userJoined(userId, socket, user) {
    const username = user ? user.username : null;
    logger.debug('userJoined', userId, user);
    this.users[userId] = {
      username,
      socket,
      roomId: null,
    };
    this.sendToUser(userId, { tag: 'hello', user });
  }

  userLeft(userId) {
    const user = this.users[userId];
    const { username, roomId } = user;

    if (roomId) {
      this.userLeavesRoom(userId, username, roomId);
    }

    logger.debug('userLeft', userId);
    delete this.users[userId];
  }

  async userGotName(userId, newUsername) {
    const user = this.users[userId];
    if (!user) {
      logger.error('userGotName', userId, newUsername, ': user missing');
      return;
    }

    const { username, roomId } = user;
    logger.debug('userGotName', userId, newUsername, 'roomId:', roomId);

    if (username) {
      logger.warn('userGotName while already having a name:', user);
    }

    if (roomId) {
      this.userLeavesRoom(userId, username, roomId);
    }
    user.username = newUsername;
    if (roomId) {
      await this.userEntersRoom(userId, newUsername, roomId);
    }
  }

  async userLostName(userId) {
    logger.debug('userLostName', userId);
    const user = this.users[userId];
    const { username, roomId } = user;
    if (!username) {
      logger.error('userLostName while not having a name:', user);
      return;
    }

    if (roomId) {
      this.userLeavesRoom(userId, username, roomId);
    }
    user.username = null;

    if (roomId) {
      await this.userEntersRoom(userId, null, roomId);
    }
  }

  async onTalkerMessage(message) {
    logger.debug('onTalkerMessage', message);
    const { global, userId } = message;

    if (global) {
      const { tag, data } = message;

      if (tag === 'game-finished') {
        const { gid, outcome } = data;
        const game = await findGameByGid(gid);
        const { shortName } = game;
        this.notifyRoomsOnGameChanged(shortName, (room) => {
          room.onGameByShortNameFinished(shortName, outcome);
        });
        this.addGameRoomMessage(shortName, 'gameFinished', {});
      } else if (tag === 'game-created') {
        const { gid } = data;
        const game = await findGameByGid(gid);
        this.notifyLobbyRoom((lobbyRoom) => {
          lobbyRoom.onGameCreated(game);
        });
      } else if (tag === 'game-updated') {
        const { gid } = data;
        await this.reloadGameByGid(gid);
      } else if (tag === 'send-your-turn-notification') {
        const { gid, username, email } = data;
        const game = await findGameByGid(gid);
        const { shortName } = game;
        mailer.sendYourTurnEmail(username, email, {
          shortName,
        });
      }
    } else if (userId) {
      const user = this.users[userId];
      if (!user) {
        logger.warn('Ignoring talker message to unknown user', userId);
        return;
      }

      const { roomId } = user;

      const room = this.rooms[roomId];
      if (!room) {
        logger.warn(
          'Ignoring talker message to user not in room',
          userId,
          roomId
        );
        return;
      }
      if (!room.isGameRoom()) {
        logger.warn(
          'Ignoring talker message to user not in game room',
          userId,
          roomId
        );
        return;
      }
      room.handleTalkerMessage(message);
    } else {
      logger.warn(
        'Ignoring talker message which is neither global nor to userId',
        message
      );
    }
  }

  async reloadGameByGid(gid) {
    const game = await findGameByGid(gid);
    const { shortName } = game;
    this.notifyRoomsOnGameChanged(shortName, (room) => {
      room.onGameByShortNameUpdated(shortName, game);
    });
  }

  async finishGameByGid(gid, outcome) {
    await finishGameAndSetOutcome(gid, outcome);

    const game = await findGameByGid(gid);
    const { shortName } = game;
    this.notifyRoomsOnGameChanged(shortName, (room) => {
      room.onGameByShortNameFinished(shortName, outcome);
    });
    this.addGameRoomMessage(shortName, 'gameFinished', {});
  }
}

export function createRoomManager(publish) {
  return new RoomManager(publish);
}
