export function gameHasPlayer(game, playerName) {
  return !!game.players.find((p) => p === playerName);
}

export function gameHasRuntimeState(game) {
  const { status } = game;
  const statuses = 'running abandoned voided finished'.split(' ');
  return !!statuses.find((s) => s === status);
}
