import { addRoomMessage } from './db';
import logger from './logger';

export class BaseRoom {
  publish: (message: object) => void;
  roomId: string;
  usersByUsername: Map<string, { [username: string]: any }>;
  io: any; // something from socket.io
  roomMessages: any[]; // TODO

  constructor(roomId, publish, io = null) {
    this.publish = publish;
    this.roomId = roomId;
    this.usersByUsername = new Map(); // username to userId to stuff
    // username may be null; we still keep all enterers here.
    this.io = io;
  }

  isGameRoom() {
    return false;
  }

  initIo(io) {
    if (this.io) {
      throw 'Already has io';
    }
    this.io = io;
  }

  getAllUsernames() {
    return [...this.usersByUsername.keys()].filter((k) => k !== null);
  }

  getAllUserIds() {
    const result = [].concat(
      ...[...this.usersByUsername.values()].map((h) => {
        console.log('one', h, Object.keys(h));
        return Object.keys(h);
      })
    );
    return result;
  }

  async userEnters(userId, username) {
    const users = this.usersByUsername.get(username);
    if (users) {
      if (users[userId]) {
        console.warn(
          `userEnters with identical userId: ${userId} ${username}; overwriting`
        );
      }
      users[userId] = 1;
    } else {
      this.usersByUsername.set(username, {
        [userId]: 1,
      });
      if (username) {
        this.notifyAll({ tag: 'userEnters', username });
      }
    }

    const baseRoomData = {
      usernames: this.getAllUsernames(),
      roomMessages: this.roomMessages,
    };
    const additionalRoomData = await this.loadInitialRoomDataFor(username);
    const roomData = {
      ...baseRoomData,
      ...additionalRoomData,
    };

    this.sendToUser(userId, {
      tag: 'initialRoomData',
      roomData,
    });
  }

  async load() {
    logger.warn('Override load in specific rooms');
  }

  async loadInitialRoomDataFor(_username) {
    return {};
  }

  notifyAll(message) {
    logger.debug('room notifyAll', this.roomId, message);
    this.io
      .to(this.roomId)
      .emit('message', { ...message, roomId: this.roomId });
  }

  notifyAllDynamic(createMessageForUserByUsername) {
    this.getAllUsernames().forEach((username) => {
      const message = createMessageForUserByUsername(username);

      const users = this.usersByUsername.get(username);
      Object.keys(users).forEach((userId) => {
        this.io.to(userId).emit('message', message);
      });
    });
  }

  sendToUser(userId, message) {
    message.roomId = this.roomId;
    this.io.to(userId).emit('message', message);
  }

  isEmpty() {
    return this.usersByUsername.size === 0;
  }

  userLeaves(userId, username) {
    const users = this.usersByUsername.get(username);
    if (users) {
      if (!userId) {
        // do nothing
      } else if (users[userId]) {
        delete users[userId];

        if (Object.keys(users).length === 0) {
          this.usersByUsername.delete(username);
          this.notifyAll({ tag: 'userLeaves', username });
        }
      } else {
        console.warn(
          `userLeaves while not in the room: ${userId}, ${username}, ${this.getAllUserIds()}`
        );
      }
    } else {
      console.warn(
        `userLeaves while not in the room: ${userId}, ${username}, ${this.getAllUserIds()}`
      );
    }
  }

  willDestroy() {
    logger.debug('room willDestroy', this.roomId);
  }

  async userSendsChatLine(userId, username, text) {
    const roomMessage = await addRoomMessage(this.roomId, 'chat', {
      author: username,
      text,
    });
    this.roomMessageAdded(roomMessage);
  }

  roomMessageAdded(roomMessage) {
    this.roomMessages.push(roomMessage);
    this.notifyAll({
      tag: 'newRoomMessage',
      roomMessage,
    });
  }
}
