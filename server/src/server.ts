import http from 'http';
import https from 'https';
import express from 'express';
import { Server } from 'socket.io';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import expressFileUpload, { UploadedFile } from 'express-fileupload';
import path from 'path';
import process from 'process';
import redis from 'redis';
import connectRedis from 'connect-redis';
import fs from 'fs';
import { createRoomManager } from './roomManager';
import childProcess from 'child_process';
import {
  getCountOfGames,
  findUserDataByUsernameOrEmail,
  findUserDataByExactUsername,
  findUserDataByEmail,
  findUsernamesByPrefix,
  setUserLastSeenNow,
  setPasswordHashOfUserByEmail,
  createUser,
  findPendingRegistrationByCode,
  findPendingRegistrationByEmail,
  confirmPendingRegistrationByCode,
  createPendingRegistration,
  closeDb,
  findPasswordRecoveryByEmail,
  createPasswordRecovery,
  deletePasswordRecoveryByEmail,
  updateUserAccountSettings,
  setPersonalNotesContent,
  getPastGamesForUser,
  getSandboxGamesForUser,
  findGameByShortName,
} from './db';
import {
  checkPasswordAgainstHash,
  generatePasswordHash,
  validatePasswordStrength,
} from './passwords';
import {
  generatePendingRegistrationCode,
  generatePasswordResetCode,
} from './pendingRegistrations';
import { mailer } from './mailer';
import { createTerminus } from '@godaddy/terminus';
import config from './config';
import logger from './logger';
import { calculateStatsForOptionalUser } from './stats';
import { gameHasPlayer } from './utils';
import { clientServerCommonData } from '@cr/engine';

const PORT = 4001;

const useSecureCookies = process.env.NODE_ENV === 'production';

const app = express();
if (useSecureCookies) {
  app.set('trust proxy', true);
}

const redisClient = redis.createClient({ db: config.redisDb });
const RedisStore = connectRedis(session);
const redisSubscriber = redis.createClient({ db: config.thmRedisDb });

const cookieParserInstance = cookieParser(config.cookieSecret);

function publish(message) {
  redisClient.publish('outside->talker', JSON.stringify(message));
}

declare module 'express-session' {
  interface SessionData {
    username?: string;
  }
}

const sessionInstance = session({
  secret: config.cookieSecret,
  store: new RedisStore({ client: redisClient }),
  cookie: {
    maxAge: 2 * 7 * 24 * 60 * 60 * 1000,
    secure: useSecureCookies,
    httpOnly: false,
  },
  name: 's',
  resave: false,
  saveUninitialized: false,
});

const roomManager = createRoomManager(publish);

redisSubscriber.on('message', (channel, message) => {
  const obj = JSON.parse(message);
  roomManager.onTalkerMessage(obj);
});
redisSubscriber.subscribe('talker->outside');

app.use(helmet());
app.use(cookieParserInstance);
app.use(bodyParser.json());
app.use(sessionInstance);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'));
app.use(expressFileUpload());

const server = http.createServer(app);

function onSignal() {
  logger.debug('signal received');
  return Promise.all([closeDb()]);
}

createTerminus(server, {
  signal: 'SIGINT',
  onSignal,
});

createTerminus(server, {
  onSignal,
});

app.post('/login', async function (req, res, _next) {
  const { usernameOrEmail, password, clientSocketId } = req.body;
  const userData = await findUserDataByUsernameOrEmail(usernameOrEmail);
  if (userData === null) {
    res.status(404).end();
  } else {
    const { user, passwordHash } = userData;
    if (checkPasswordAgainstHash(password, passwordHash)) {
      const { username } = user;
      req.session.username = username;
      const userId = clientSocketId;
      await roomManager.userGotName(userId, username);

      setUserLastSeenNow(username);
      res.json(user);
    } else {
      res.status(401).end();
    }
  }
});

app.post('/logout', async function (req, res, _next) {
  const { username } = req.session;
  if (!username) {
    res.status(401).end();
    return;
  }

  const { clientSocketId } = req.body;

  const userId = clientSocketId;
  roomManager.userLostName(userId, username);

  delete req.session.username;
  res.status(200).end();
});

app.post('/signup', async function (req, res, _next) {
  if (req.session.username) {
    res.status(401).end();
    return;
  }
  const { email } = req.body;
  const userData = await findUserDataByEmail(email);
  if (userData) {
    res.status(403).json({ error: 'emailTaken' }).end();
    return;
  }

  const pendingRegistration = await findPendingRegistrationByEmail(email);
  if (pendingRegistration) {
    res.status(403).json({ error: 'pendingRegistrationExists' }).end();
    return;
  }

  const code = generatePendingRegistrationCode();
  const created = await createPendingRegistration({
    email,
    code,
  });

  if (!created) {
    res.status(500).end();
    return;
  }

  const result = await mailer.sendSignupEmail(email, code);
  if (result) {
    res.status(200).end();
  } else {
    res.status(400).end();
  }
});

function validateUsername(username, validationErrors) {
  const { usernameMinLength, usernameMaxLength } =
    clientServerCommonData.validationConstants;
  if (!(username.length >= usernameMinLength)) {
    validationErrors.push({
      tag: 'usernameTooShort',
      req: usernameMinLength,
      field: 'username',
    });
  }
  if (!(username.length <= usernameMaxLength)) {
    validationErrors.push({
      tag: 'usernameTooLong',
      req: usernameMaxLength,
      field: 'username',
    });
  }

  // Latin; letters from Latin Supplement; Latin Extended-A and -B
  const re = /^[-_ 0-9A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u024F]*$/;
  if (!re.test(username)) {
    validationErrors.push({
      tag: 'usernameIllegalCharacters',
      field: 'username',
    });
  }

  return validationErrors.length === 0;
}

app.post('/complete-signup', async function (req, res, _next) {
  /* eslint-disable prefer-const */
  let {
    code,
    email,
    username,
    password,
    repeatPassword,
    allowGameEmails,
    clientSocketId,
  } = req.body;
  /* eslint-enable prefer-const */

  email = email.trim();
  username = username.trim();

  if (req.session.username) {
    res.status(401).end();
    return;
  }

  const validationErrors = [];

  const pendingRegistration = await findPendingRegistrationByCode(code);
  if (!pendingRegistration) {
    validationErrors.push({
      tag: 'noPendingRegistration',
    });
  } else if (pendingRegistration.email !== email) {
    validationErrors.push({
      tag: 'wrongPendingRegistrationEmail',
      field: 'email',
    });
  } else if (pendingRegistration.isConfirmed) {
    validationErrors.push({
      tag: 'wrongPendingRegistrationEmail',
      field: 'email',
    });
  }

  // At this point, something is wrong with the code, so don't validate
  // further.
  if (validationErrors.length > 0) {
    res.status(403).json(validationErrors).end();
    return;
  }

  validatePasswordStrength(password, validationErrors);

  if (password !== repeatPassword) {
    validationErrors.push({
      tag: 'passwordsDoNotMatch',
      field: 'repeatPassword',
    });
  }

  validateUsername(username, validationErrors);

  // Take care of case, and a remote possibility of an email looking like a
  // username.
  const user1 = await findUserDataByUsernameOrEmail(username);
  const user2 = await findUserDataByUsernameOrEmail(email);
  if (user1 || user2) {
    validationErrors.push({ tag: 'usernameTaken', field: 'username' });
  }

  if (validationErrors.length > 0) {
    res.status(403).json(validationErrors).end();
    return;
  }

  // XXX: should wrap all db work in a transaction.
  const passwordHash = generatePasswordHash(password);
  const created = await createUser({
    username,
    passwordHash,
    email,
    allowGameEmails,
  });

  if (!created) {
    res.status(500).json({ error: 'cannot create user' }).end();
    return;
  }

  await confirmPendingRegistrationByCode(pendingRegistration.code);

  const userData = await findUserDataByUsernameOrEmail(email);
  if (!userData) {
    logger.error();
    res.status(500).json({ error: 'cannot find user after creation' }).end();
    return;
  }
  const { user } = userData;
  req.session.username = user.username;
  const userId = clientSocketId;
  roomManager.userGotName(userId, username);

  res.status(200).json(user).end();
});

app.post('/request-password-reset', async function (req, res, _next) {
  const { username } = req.session;
  if (username) {
    res.status(401).end();
    return;
  }

  const { usernameOrEmail } = req.body;
  const userData = await findUserDataByUsernameOrEmail(usernameOrEmail);
  if (!userData) {
    res.status(403).json({ tag: 'userNotFound' }).end();
    return;
  }

  const { user } = userData;
  const { email } = user;

  const passwordRecovery = await findPasswordRecoveryByEmail(email);
  if (passwordRecovery) {
    res.status(403).json({ tag: 'passwordRecoveryInProgress' }).end();
    return;
  }

  const code = generatePasswordResetCode();
  const created = await createPasswordRecovery({ email, code });
  if (!created) {
    res.status(500).json({ error: 'cannotCreatePasswordRecovery' }).end();
    return;
  }

  const result = await mailer.sendResetEmail(user.username, email, code);
  if (result) {
    res.status(200).end();
  } else {
    res.status(400).end();
  }
});

app.post('/complete-password-reset', async function (req, res, _next) {
  /* eslint-disable prefer-const */
  let { usernameOrEmail, password, repeatPassword, code, clientSocketId } =
    req.body;
  /* eslint-enable prefer-const */

  usernameOrEmail = usernameOrEmail.trim();

  if (req.session.username) {
    res.status(401).end();
    return;
  }

  const validationErrors = [];
  let user, email;

  const userData = await findUserDataByUsernameOrEmail(usernameOrEmail);
  if (!userData) {
    validationErrors.push({
      tag: 'passwordRecovery_userNotFound',
      field: 'usernameOrEmail',
    });
  } else {
    user = userData.user;
    email = user.email;

    const passwordRecovery = await findPasswordRecoveryByEmail(email);
    if (!passwordRecovery) {
      validationErrors.push({
        tag: 'passwordRecovery_noPasswordRecoveryInProgress',
        field: 'usernameOrEmail',
      });
    } else if (passwordRecovery.code !== code) {
      validationErrors.push({ tag: 'passwordRecovery_invalidCode' });
    }
  }

  validatePasswordStrength(password, validationErrors);

  if (password !== repeatPassword) {
    validationErrors.push({
      tag: 'passwordsDoNotMatch',
      field: 'repeatPassword',
    });
  }

  if (validationErrors.length > 0) {
    res.status(403).json(validationErrors).end();
    return;
  }

  // XXX: should wrap all db work in a transaction.

  const deleted = await deletePasswordRecoveryByEmail(email);
  if (!deleted) {
    logger.error('Cannot delete password recovery', email, code);
    res.status(500).json({ error: 'cannotDeletePasswordRecovery' }).end();
    return;
  }

  const passwordHash = generatePasswordHash(password);
  const updated = await setPasswordHashOfUserByEmail(passwordHash, email);
  if (!updated) {
    logger.error('Cannot update password hash', email, code);
    res.status(500).json({ error: 'cannotUpdatePasswordHash' }).end();
    return;
  }

  const { username } = user;
  req.session.username = username;
  const userId = clientSocketId;
  roomManager.userGotName(userId, username);

  res.status(200).json(user).end();
});

app.post('/change-account-settings', async function (req, res, _next) {
  const { allowGameEmails } = req.body;

  const { username } = req.session;
  if (!username) {
    res.status(401).end();
    return;
  }

  const updated = await updateUserAccountSettings(username, allowGameEmails);

  if (!updated) {
    logger.error('Cannot update account settings', username, allowGameEmails);
    res.status(500).json({ error: 'cannotUpdateAccountSettings' }).end();
    return;
  }

  res.status(200).end();
});

app.post('/check-invitee', async function (req, res, _next) {
  const { invitee } = req.body;
  const { username } = req.session;

  const invitableUsernames = (await findUsernamesByPrefix(invitee)).filter(
    (potentialInviteeUsername) => potentialInviteeUsername !== username
  );

  const found = invitableUsernames.find((username) => {
    return username.toLowerCase() === invitee.toLowerCase();
  });

  const nUsernames = invitableUsernames.length;
  // XXX that could've been done better in the database, but then a query
  // should return both the count and the array slice, and I don't know how to
  // do that.
  const limit = 15;

  const usernames = invitableUsernames.slice(0, limit);

  const validationErrors = [
    {
      tag: 'inviteeDoesNotExist',
      field: 'invitee',
    },
  ];

  return res
    .status(200)
    .json({ found, validationErrors, usernames, nUsernames })
    .end();
});

function getAvatarFilename(username) {
  return `${config.avatarsDirectory}/${username}.png`;
}

function getRoboUrl(username) {
  return `https://robohash.org/${username}.png?bgset=bg2`;
}

function downloadAvatar(username) {
  logger.info('downloading avatar', username);
  const targetFilename = getAvatarFilename(username);
  const url = getRoboUrl(username);
  const file = fs.createWriteStream(targetFilename);
  // XXX we don't handle the errors here, but such is life: remember to check
  // it if we're with corrupted avatar images sometime.
  const _request = https.get(url, function (response) {
    response.pipe(file);
  });
}

app.get('/avatar/:username.png', function (req, res, next) {
  const { username } = req.params;

  if (!validateUsername(username, [])) {
    res.status(401).end();
    return;
  }

  if (!fs.existsSync(getAvatarFilename(username))) {
    downloadAvatar(username);
    res.redirect(getRoboUrl(username));
    return;
  }

  next();
});

app.use('/avatar', express.static(config.avatarsDirectory));

app.post('/upload-avatar', (req, res) => {
  const { username } = req.session;
  if (!username) {
    res.status(401).end();
    return;
  }

  if (!req.files) {
    res.status(400).end();
    return;
  }

  // Not sure why it's understood as UploadedFile[], but the JS code was
  // working before.
  const file = req.files.avatar as UploadedFile;

  if (!(file.size <= 2 * 1024 * 1024)) {
    res.status(400).end();
    return;
  }

  const [_, extension] = file.name.match(/\.(.*)$/);

  logger.info('uploading avatar', username, file);

  const targetFilename = getAvatarFilename(username);
  const rawFilename = targetFilename + '.raw.' + extension;

  const escape = (x) => "'" + x.replace(/'/g, "'\\''") + "'";
  file.mv(rawFilename, function (err) {
    if (err) {
      logger.error('error copying the uploaded avatar', err);
      res.status(500).end();
      return;
    }
    childProcess.exec(
      `convert -resize "64x64!" ${escape(rawFilename)} ${escape(
        targetFilename
      )}`,
      function (error, stdout, stderr) {
        if (error) {
          logger.error(
            'error converting uploaded avatar',
            rawFilename,
            targetFilename,
            error,
            stderr
          );
          res.status(500).end();
          return;
        } else {
          res.status(200).end();
        }
      }
    );
  });
});

app.post('/set-personal-notes-content', async (req, res) => {
  const { username } = req.session;
  if (!username) {
    res.status(401).end();
    return;
  }

  const { shortName, content } = req.body;
  if (!(shortName && content)) {
    return res.status(403).end();
  }

  const game = await findGameByShortName(shortName);
  if (!game) {
    return res.status(404).end();
  }

  if (!gameHasPlayer(game, username)) {
    return res
      .status(403)
      .json({ error: 'Must be in game to take notes' })
      .end();
  }

  const { gid } = game;

  const result = await setPersonalNotesContent(username, gid, content);
  if (result) {
    res.status(200).end();
  } else {
    res.status(500).end();
  }
});

app.get('/past-games', async function (req, res, _next) {
  const { username } = req.session;
  if (!username) {
    res.status(401).end();
    return;
  }

  const games = await getPastGamesForUser(username);
  res.status(200).json(games).end();
});

app.get('/sandbox-games', async function (req, res, _next) {
  const { username } = req.session;
  if (!username) {
    res.status(401).end();
    return;
  }

  const games = await getSandboxGamesForUser(username);
  res.status(200).json(games).end();
});

app.get('/stats', async function (req, res, _next) {
  const { username } = req.session;

  const games = await calculateStatsForOptionalUser(username);
  res.status(200).json(games).end();
});

app.get('/*', function (req, res, _next) {
  res.sendFile(path.resolve('public/index.html'));
});

const socketIoOptions =
  process.env.NODE_ENV === 'development'
    ? {
        cors: {
          origin: '*',
        },
      }
    : {};

const io = new Server(server, socketIoOptions);
roomManager.initIo(io);

function getSessionFromSocketProd(socket) {
  return new Promise((resolve, reject) => {
    try {
      // Invoke two middlewares to get session at the end.  Type as "any" to
      // silence Typescript; probably could construct those requests in a more
      // proper way.
      const constructedRequest: any = {};
      for (const [k, v] of Object.entries(socket.handshake)) {
        if (k === 'headers' || k === 'url') {
          constructedRequest[k] = v;
        }
      }
      const constructedResponse: any = {};
      cookieParserInstance(constructedRequest, constructedResponse, () => {
        sessionInstance(constructedRequest, constructedResponse, () => {
          const { session } = constructedRequest;
          resolve(session);
        });
      });
    } catch (e) {
      reject(e);
    }
  });
}

function getSessionFromSocketLocalDev(socket) {
  return new Promise((resolve, reject) => {
    try {
      const { cookie } = socket.handshake.auth;

      const constructedRequest: any = {
        headers: {
          cookie,
        },
        originalUrl: '/',
      };
      const constructedResponse: any = {};

      sessionInstance(constructedRequest, constructedResponse, () => {
        const session = constructedRequest.session;
        resolve(session);
      });
    } catch (e) {
      reject(e);
    }
  });
}

io.on('connection', async (socket) => {
  const timestamp = Date.now();
  const userId = socket.id;

  let session;
  try {
    if (useSecureCookies) {
      session = await getSessionFromSocketProd(socket);
    } else {
      session = await getSessionFromSocketLocalDev(socket);
    }
  } catch (e) {
    logger.warn('Cannot get session from socket', e);
    session = {};
  }

  const { username = null } = session;

  let user = null;
  if (username) {
    const userData = await findUserDataByExactUsername(username);
    if (!userData) {
      logger.warn('no userData for username', username);
      user = null;
    } else {
      setUserLastSeenNow(username);
      user = userData.user;
    }
  }

  logger.debug('socketio connection open', userId, timestamp, username);
  roomManager.userJoined(userId, socket, user);

  socket.on('message', async (message) => {
    await roomManager.userSentMessage(userId, message);
  });

  socket.on('disconnect', () => {
    logger.debug('socketio connection close', userId);
    roomManager.userLeft(userId);
  });
});

getCountOfGames().then((n) => {
  logger.debug('getCountOfGames', n);
});

server.listen(PORT);
logger.log('Listening on port', PORT, process.env.NODE_ENV);
