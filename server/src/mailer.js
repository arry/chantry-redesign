import fs from 'fs';
import config from './config';
import logger from './logger';
import mailgunFactory from 'mailgun-js';
import mustache from 'mustache';

const mailgun = mailgunFactory({
  apiKey: config.mailgunApiKey,
  domain: config.mailgunDomain,
});

const _baseTemplate = fs.readFileSync(
  'resources/emails/email-inlined.html.mustache',
  'utf8'
);
const noFooterTemplate = fs.readFileSync(
  'resources/emails/nofooter-inlined.html.mustache',
  'utf8'
);

async function sendEmail(template, values, email, subject) {
  const html = mustache.render(template, values);

  const data = {
    from: 'Chantry <auto@chantry-games.com>',
    to: email,
    subject,
    html,
  };
  try {
    const response = await mailgun.messages().send(data);
    logger.info('Sent email', email, subject);
    logger.log(response);
    return true;
  } catch (e) {
    logger.info('Failed to send email', email, subject);
    return false;
  }
}

function greeting(username) {
  return `Hi ${username},`;
}

const unsubscribeHack =
  'If you no longer wish to receive these emails, please login to your account and uncheck the “Allow emails from async games” checkbox.';

export const mailer = {
  async sendSignupEmail(email, code) {
    const values = {
      title: 'Chantry sign-up confirmation',
      greeting: 'Welcome to Chantry!',
      preCtaText: 'To finish your sign-up, please click this button:',
      ctaUrl: `${config.baseUrl}/complete-signup/${code}`,
      ctaButtonText: 'Complete sign-up',
      postCtaText:
        "If you didn’t want to sign up for Chantry (a site to play board games online), sorry! We won't bug you again.",
      footerText: '',
      unsubscribeUrl: '',
      preUnsubscribeText: '',
    };
    return sendEmail(
      noFooterTemplate,
      values,
      email,
      'Chantry sign-up confirmation'
    );
  },

  async sendResetEmail(username, email, code) {
    const values = {
      title: 'Reset password',
      greeting: greeting(username),
      preCtaText: 'To complete the password reset, please click this button:',
      ctaUrl: `${config.baseUrl}/complete-password-reset/${code}`,
      ctaButtonText: 'Complete password reset',
      postCtaText:
        'If you didn’t request a password reset, sorry: someone else did it by mistake.',
      footerText: '',
      unsubscribeUrl: '',
      preUnsubscribeText: '',
    };

    return sendEmail(
      noFooterTemplate,
      values,
      email,
      'Chantry - reset password'
    );
  },

  async sendPlayerJoinsEmail(username, email, data) {
    const { shortName, joinerUsername } = data;
    const shortSubject = `${joinerUsername} joins ${shortName}`;
    const values = {
      title: shortSubject,
      greeting: greeting(username),
      preCtaText: `${joinerUsername} joins your game ${shortName}.  It's ready to start now.`,
      ctaButtonText: 'Go to game',
      ctaUrl: `${config.baseUrl}/game/${shortName}`,
      postCtaText: unsubscribeHack,
      footerText: '',
      unsubscribeUrl: '',
      preUnsubscribeText: '',
    };
    return sendEmail(
      noFooterTemplate,
      values,
      email,
      `Chantry - ${shortSubject}`
    );
  },

  async sendPlayerLeavesEmail(username, email, data) {
    const { shortName, leavingUsername } = data;
    const shortSubject = `${leavingUsername} leaves ${shortName}`;
    const values = {
      title: shortSubject,
      greeting: greeting(username),
      preCtaText: `${leavingUsername} leaves your game ${shortName}.  It can no longer be started`,
      ctaButtonText: 'Go to game',
      ctaUrl: `${config.baseUrl}/game/${shortName}`,
      postCtaText: unsubscribeHack,
      footerText: '',
      unsubscribeUrl: '',
      preUnsubscribeText: '',
    };
    return sendEmail(
      noFooterTemplate,
      values,
      email,
      `Chantry - ${shortSubject}`
    );
  },

  async sendYourTurnEmail(username, email, data) {
    const { shortName } = data;
    const shortSubject = `Your turn in ${shortName}`;
    const values = {
      title: shortSubject,
      greeting: greeting(username),
      preCtaText: `It's your turn in the game ${shortName}.`,
      ctaButtonText: 'Go to game',
      ctaUrl: `${config.baseUrl}/play/${shortName}`,
      postCtaText: unsubscribeHack,
      footerText: '',
      unsubscribeUrl: '',
      preUnsubscribeText: '',
    };
    return sendEmail(
      noFooterTemplate,
      values,
      email,
      `Chantry - ${shortSubject}`
    );
  },
};
