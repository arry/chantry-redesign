// This is the build as in "build ts files to js using esbuild".

const esbuild = require('esbuild');
const alias = require('esbuild-plugin-alias');
const path = require('path');

const watch = process.argv[2] === '--watch';

esbuild
  .build({
    entryPoints: ['src/server.ts'],
    bundle: true,
    outfile: 'dist/index.js',
    platform: 'node',
    watch,
    logLevel: 'info',
    plugins: [
      alias({
        '@cr/engine': path.resolve(__dirname, '../../engine/src/main.ts'),
      }),
    ],
    sourcemap: true,
    external: ['pg-native'],
  })
  .catch(() => process.exit(1));
