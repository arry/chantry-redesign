const bcrypt = require('bcryptjs');

function generatePasswordHash(password) {
    return bcrypt.hashSync(password);
}

console.log(process.argv);

const username = process.argv[2];

if (!username) {
    console.error('usage: gen-password <username>');
    process.exit(1);
}

const passwordHash = generatePasswordHash(username);

console.log(
    `insert into users (username, password_hash, email) values ('${username}', '${passwordHash}', '${username}@mailinator.com');`
);
