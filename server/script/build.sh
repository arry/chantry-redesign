#!/bin/sh

# This is build as in "prepare files for deployment"; and it depends on
# esbuild's "build.js" config.

rm -rf build
mkdir build
yarn build
cp -R resources build
cp -R dist/* build
