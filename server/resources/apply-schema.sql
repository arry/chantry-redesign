CREATE TABLE users (
    username TEXT PRIMARY KEY,
    password_hash TEXT NOT NULL,
    email TEXT NOT NULL UNIQUE,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    has_avatar BOOLEAN DEFAULT FALSE,
    last_seen_at TIMESTAMP DEFAULT current_timestamp,
    account_settings jsonb null,
    game_settings_on_last_creation jsonb not null default jsonb('{}')
);

CREATE TABLE pending_registrations (
    email TEXT NOT NULL UNIQUE,
    confirmation_code TEXT NOT NULL,
    is_confirmed BOOLEAN DEFAULT FALSE
);

CREATE TABLE password_recoveries (
    email TEXT NOT NULL UNIQUE,
    recovery_code TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE OR REPLACE FUNCTION update_last_modified()
RETURNS TRIGGER AS $$
BEGIN
    NEW.last_modified_at = now();
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TYPE game_time_kind AS ENUM ('live', 'async');

CREATE TABLE games (
    gid serial PRIMARY KEY,
    code varchar(10) not null,
    kind game_time_kind null default 'live',
    status varchar(12) not null,
    created_at timestamp not null default now(),

    owner_username text references users (username),
    comment varchar(100) null,
    game_settings jsonb null,
    players varchar(20)[] not null,
    last_modified_at timestamp not null default now(),

    runtime_info jsonb null,
    outcome jsonb null
);

CREATE TRIGGER update_games_modtime
BEFORE UPDATE ON games
FOR EACH ROW EXECUTE PROCEDURE update_last_modified();

create table game_engine_checkpoints (
    gid integer unique references games(gid) on delete cascade,
    engine_checkpoint bytea
);


create table personal_notes (
   username text references users (username) on delete cascade,
   gid integer unique references games(gid) on delete cascade,
   content varchar(2048)
);

alter table personal_notes add constraint personal_notes_username_gid unique (username, gid);

alter table personal_notes drop constraint personal_notes_gid_key;

create table room_messages (
   room_id text not null,
   created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
   message_code text not null,
   args jsonb null
);

create sequence games_short_name_seki_seq as integer;

create sequence games_short_name_td_seq as integer;
select setval('games_short_name_td_seq', (select last_value from games_gid_seq));

alter table games add column short_name varchar(20);
update games set short_name=concat(code, '-', gid);
alter table games alter column short_name set not null;

alter type game_time_kind add value 'sandbox';
