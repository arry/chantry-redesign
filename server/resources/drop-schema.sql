drop table room_messages;

drop table personal_notes;
drop table game_engine_checkpoints;

drop trigger update_games_modtime on games;
drop table games;
drop type game_time_kind;

drop function update_last_modified();

drop table password_recoveries;
drop table pending_registrations;
drop table users;
