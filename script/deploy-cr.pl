#!/usr/bin/env perl

use strict;
use warnings;
use 5.010;

use File::Spec::Functions qw(catfile);
use File::Path qw(make_path);
use Term::ANSIColor;
use FindBin;

my $tmp_dir = "build-tmp";

my $remote = "arry\@138.68.81.128"; # should take from from config

my $task = shift // 'all';

if ($task eq 'all') {
    make_build_dir();
}

sub report {
    my $color = shift;
    my $text = shift;
    print color $color;
    print "$text\n";
    print color 'reset';
}

sub notify {
    my $text = shift;
    system("notify-send -t 1500 '$text'")
}

if ($task eq 'client' || $task eq 'all') {
    report('yellow', "Building client");
    build_client();
    copy_client();
}

if ($task eq 'server' || $task eq 'all') {
    report('yellow', "Building server");
    build_server();
    copy_server();
}

if ($task eq 'remote-client' || $task eq 'all') {
    report('yellow', 'Ready to push client to remote');
    notify('Ready to push client to remote');
    copy_client_build_to_remote();

    # restart_services();
}

if ($task eq 'remote-server' || $task eq 'all') {
    report('yellow', 'Ready to push server to remote');
    notify('Ready to push server to remote');
    copy_server_build_to_remote();
}

if ($task eq 'remote-commands' || $task eq 'all') {
    report('yellow', 'Ready to install deps and restart services');
    notify('Ready to install deps and restart services');
    my @commands = (
        '. ~/.nvm/nvm.sh && pm2 restart cr',
    );
    for my $cmd (@commands) {
        my $command = "ssh $remote '$cmd'";
        warn "run: $command\n";
        wrap_system("$command");
    }
}

report_all_right();

sub make_build_dir {
    make_path($tmp_dir);
    # try not deleting contents?
    # for (<$tmp_dir/*>) {
    #     wrap_system("rm -rf $_");
    # }
}

sub wrap_system {
    my @args = @_;
    my $result = system @args;
    unless ($result == 0) {
        warn "Failed to execute `@args'\n";
        print color 'red';
        print "Deployment failed\n";
        print color 'reset';
        notify('Deployment failed');
        exit $result;
    }
}


sub copy {
    my $src_dir = shift;
    my $target_dir = shift;
    my @specs = @_;

    for my $spec (@specs) {
        my ($file_name, @args);
        if (ref $spec eq 'ARRAY') {
            ($file_name, @args) = @{ $spec };
        }
        else {
            $file_name = $spec;
        }
        my $src = catfile($src_dir, $file_name);
        if (-d $src) {
            $src = "$src/";
        }
        my $dest = catfile($target_dir, $file_name);
        my $joined_args = join ' ', @args;
        my $command = <<"END";
rsync -az --exclude '*.orig' @args $src $dest
END
        warn "run: $command\n";
        wrap_system($command);
    }
}

sub build_client {
    wrap_system("cd client && npm run build && cd ..");
}

sub copy_client {
    wrap_system("rsync -a --delete client/build/ $tmp_dir/client/");

    # Bring back the original timestamps lost by c-r-a build
    wrap_system("rsync -avz --exclude=index.html client/public/ $tmp_dir/client")
}

sub build_server {
    wrap_system("cd server && ./script/build.sh && cd ..");
}

sub copy_server {
    wrap_system("rsync -a --delete server/build/ $tmp_dir/server/");
}

sub copy_client_build_to_remote {
    wrap_system("rsync -avz --progress ${tmp_dir}/client/ $remote:/var/www/cr/");
}

sub copy_server_build_to_remote {
    my $base = "$remote:/opt/cr-server";
    wrap_system("rsync -avz --progress --delete ${tmp_dir}/server/resources/ ${base}/resources");
    wrap_system("rsync -avz --progress ${tmp_dir}/server/* ${base}/");
}


sub report_all_right {
    print color 'green';
    print "Deployment is successful\n";
    print color 'reset';
}
